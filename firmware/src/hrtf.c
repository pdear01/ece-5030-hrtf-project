/******************************************************************************\
 * HRTF project initialization, other random functions
 * ===========================================================================
 *    ECE 5030 Head Related Transfer Function
 *  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
\******************************************************************************/

#include <inttypes.h>
#include <msp430.h>
#include "hrtf.h"
#include "uart.h"
#include "adc.h"
#include "ram.h"
#include "dac.h"


void hrtf_init(void) {
    // Turn off watchdog
    WDTCTL = WDTPW | WDTHOLD;

    // Setup clocks
    BCSCTL1 = CALBC1_16MHZ;
    DCOCTL = CALDCO_16MHZ; // MCLK = 16 MHz
    BCSCTL2 = DIVS_0; // set SMCLK to 16 MHz

    // Setup timer A0 interrupt
    TA0CTL = TASSEL_2 | ID_0 | MC_1;
    TACCTL0 = CCIE;
    TACCR0 = 999; // The number of timer counts in the period is actually 1000 cycles
    // This corresponds to a frequency of 16e6 / 1000 = 16 kHz

    // initialize other stuff
    uart_init();
    adc_init();

    // Enable interrupts
    _BIS_SR(GIE);
}

uint16_t set_timing_for_streaming(void) {
    TA0CTL = 0;
    TACCTL0 = 0;
    _BIC_SR(GIE);
    TA0CTL = TASSEL_2 | ID_0 | MC_1;
    TACCTL0 = CCIE;
	TACCR0 = 999; // timer period = 16 kHz
    _BIS_SR(GIE);
	return SAMPLING_FREQUENCY_STREAMING;
}

uint16_t set_timing_for_acquisition(void) {
    _BIC_SR(GIE);
	TACCR0 = 499; // timer period = 32 kHz
    _BIS_SR(GIE);
	return SAMPLING_FREQUENCY_ACQUIRE;
}



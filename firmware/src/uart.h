/******************************************************************************\
 * Definitions for UART module
 * ===========================================================================
 *    ECE 5030 Head Related Transfer Function
 *  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
\******************************************************************************/

#ifndef _UART_H_
#define _UART_H_

// Macros
#define UART_RX_BUFFER_SIZE      16

// Commands
enum {
    UART_COMMAND_NONE = 0,      // Haven't received complete command yet
    UART_COMMAND_INVALID,       // Recieved garbage
    UART_COMMAND_OVERFLOW,      // Detected buffer overflow
    UART_COMMAND_RESET,         // Reset MCU
    UART_COMMAND_UPTIME,        // Get uptime count
    UART_COMMAND_START_STREAM,  // Start streaming input to output, applying HRTF
    UART_COMMAND_STOP_STREAM,   // Stop streaming input to output, applying HRTF
    UART_COMMAND_SWIRL,         //
    UART_COMMAND_ANGLE,         // Set HRTF angle
    UART_COMMAND_MATLAB,        // Enter MATLAB mode
    UART_COMMAND_SAMPLE,        // Do whole sampling operation
    UART_COMMAND_GETDATA,       // Get data from sampling operation
    UART_COMMAND_GETDATA_NEXT,  // Get data from sampling operation
    UART_COMMAND_GETDATA_DONE,  // Get data from sampling operation
    UART_COMMAND_BYE,           // Exit MATLAB mode
    UART_COMMAND_TEST_RAM,      // Try writing/reading RAM
    UART_COMMAND_TEST_ADC,      // Get samples from ADC
    UART_COMMAND_TEST_DAC       //
} _uart_commands;

// Function prototypes
void uart_init(void);
void uart_write_buffer(uint8_t *, uint16_t);
void uart_write_string(const char *);
void uart_write_char(char);
void uart_write_byte(uint8_t);
void uart_write_uint32(uint32_t);
void uart_write_uint32_mindigits(uint32_t, uint8_t);
void uart_write_int32(int32_t);
uint8_t uart_get_command(uint8_t);

#endif


/******************************************************************************\
 * Subroutines for UART communication
 * ===========================================================================
 *    ECE 5030 Head Related Transfer Function
 *  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
\******************************************************************************/

#include <inttypes.h>
#include <msp430.h>
#include <string.h>
#include <stdlib.h>
#include "uart.h"
#include "int2str.h"
#include "hrtf.h"

// Declared in main.c
extern uint16_t new_hrtf_angle;

// state variables
volatile uint8_t uart_queue_valid = 0;
volatile uint8_t * uart_queue_data;
volatile uint16_t uart_queue_sent_count;
volatile uint16_t uart_queue_len;

unsigned char uart_rx_buffer[UART_RX_BUFFER_SIZE];
uint8_t uart_rx_buffer_index = 0;
uint8_t sent_prompt_string = 0;

//==============================================================================
// Initialize UART 0 for communication with sub
//==============================================================================
void uart_init(void) {
    // Use UCA0 module in UART mode, MSB first, no parity, 8-bits, 1 stop bit, asynchronous
    UCA0CTL0 = 0;

    // USE SMCLK as clock source 
    UCA0CTL1 = UCSSEL_2;

    // Use fast mode (oversampling disabled)
    // According to datasheet, for a 256000 baud rate with a 16 MHz clock,
    // the following settings should be used:
    //   UCBR = 62
    //   UCBRS0 = 0
    //   UCBRF0 = 0
    UCA0BR0 = (4 & 0xFF);
    UCA0BR1 = 0;//(62 >> 8) & 0xFF;
    UCA0MCTL = (5 << 1); // UCBRS0
    UCA0MCTL |= (3 << 4) | UCOS16; // UCBRF0

    // Setup interrupts
    IE2 |= UCA0RXIE | UCA0TXIE;
    IFG2 &= ~UCA0TXIFG; // this interrupt flag is set by default

    // clear RX buffer
    uart_rx_buffer_index = 0;
    uint8_t i;
    for (i = 0; i < UART_RX_BUFFER_SIZE; i++) {
        uart_rx_buffer[i] = 0;
    }

    // Initialize pins for UART
    P1SEL  |= 0x06;
    P1SEL2 |= 0x06;
}

//==============================================================================
// Write a single byte over uart 0 (the sub UART)
//
// Don't use this outside this file if you wish to avoid potential concurrency
// bugs.
//==============================================================================
static void _uart_write_byte(uint8_t byte) { // declared static so nobody else can access it!
    while ((UCA0STAT & UCBUSY)); // Wait until we're no longer transmitting
    UCA0TXBUF = byte;
}

//==============================================================================
// Write a string over UART
//==============================================================================
void uart_write_string(const char * str) {
    uart_write_buffer((uint8_t *)str, strlen(str));
}

//==============================================================================
// Write buffer contents out over the UART
//
// This operation is done asynchronously (i.e., the function will return before
// all of the data has been sent, and interrupts will be used to send the
// remainder of the data)
//==============================================================================
void uart_write_buffer(uint8_t * data, uint16_t len) {
    while (uart_queue_valid);
    
    if (len > 1) {
        uart_queue_len = len;
        uart_queue_data = data;
        uart_queue_sent_count = 1; // we'll send one of the bytes from this function
        uart_queue_valid = 1;
    }

    _uart_write_byte(*data);
}

//==============================================================================
// Write a single byte over the UART
//==============================================================================
void uart_write_byte(uint8_t b) {
    uart_write_buffer(&b, 1);
}

//==============================================================================
// Write a single character over the UART
//==============================================================================
void uart_write_char(char c) {
    uart_write_buffer((uint8_t *)&c, 1);
}

//==============================================================================
// Write an unsigned 32-bit integer over the UART as a string in decimal
//==============================================================================
void uart_write_uint32(uint32_t i) {
    uart_write_uint32_mindigits(i, 1);
}

//==============================================================================
// Write an unsigned 32-bit integer over the UART as a string in decimal, with
// a minimum number of digits (i.e., zeros are padded to the front if necessary)
//==============================================================================
void uart_write_uint32_mindigits(uint32_t i, uint8_t mindigits) {
    static unsigned char buffer[11];
    if (mindigits >= 11) mindigits = 10;
    uint32_to_string(i, buffer, 11);
    uint8_t empty_chars = 0;
    while (buffer[empty_chars] == ' ' && empty_chars < 11) empty_chars++;
    while (11 - empty_chars < mindigits) {
        buffer[--empty_chars] = '0';
    }
    uart_write_buffer(buffer + empty_chars, 11 - empty_chars);
}


//==============================================================================
// Write an signed 32-bit integer over the UART as a string in decimal
//==============================================================================
void uart_write_int32(int32_t i) {
    static unsigned char buffer[11];
    int32_to_string(i, buffer, 11);
    uint8_t empty_chars = 0;
    while (buffer[empty_chars] == ' ' && empty_chars < 11) empty_chars++;
    uart_write_buffer(buffer + empty_chars, 11 - empty_chars);
}

//==============================================================================
// Get the most recently received command.
//==============================================================================
uint8_t uart_get_command(uint8_t matlab_mode) {
    uint8_t command = UART_COMMAND_INVALID;
    
    // If there's nothing in the buffer, return NONE
    if (!uart_rx_buffer_index) {
        if (!sent_prompt_string && !matlab_mode) {
            uart_write_buffer((uint8_t *)"> ", 2);
            sent_prompt_string = 1;
        }
        return UART_COMMAND_NONE;
    }

    // Convert everything to lower case, make sure there's actually a newline
    // character delineating a command
    uint8_t i;
    uint8_t found_newline = 0;
    for (i = 0; i < uart_rx_buffer_index; i++) {
        if (uart_rx_buffer[i] == '\n') {
            found_newline = i+1; // Set found_newline flag to newline character index plus 1
        }
        if (uart_rx_buffer[i] >= 'A' && uart_rx_buffer[i] <= 'Z')
            uart_rx_buffer[i] += ('a' - 'A');
    }

    // If we haven't completed a command yet, return NONE (or reset stuff if we detect
    // the buffer will overflow)
    if (!found_newline) {
        if (uart_rx_buffer_index == UART_RX_BUFFER_SIZE-1) {
            command = UART_COMMAND_OVERFLOW;
            goto reset; // That's right, I used goto; deal with it
        }
        else
            return UART_COMMAND_NONE;
    }

    found_newline--; // reset found_newline to actual newline character index
    if (found_newline != 0)
        uart_rx_buffer[found_newline] = '\0';
    
    // Testing commands
    if (strcmp("testadc",(char *)uart_rx_buffer) == 0) {
        command = UART_COMMAND_TEST_ADC;
    }
    if (strcmp("testdac",(char *)uart_rx_buffer) == 0) {
        command = UART_COMMAND_TEST_DAC;
    }
    else if (strcmp("testram", (char *)uart_rx_buffer) == 0) {
        command = UART_COMMAND_TEST_RAM;
    }

    // Diagnostic commands
    else if (strcmp("reset", (char *)uart_rx_buffer) == 0) {
        command = UART_COMMAND_RESET;
    }
    else if (strcmp("uptime",(char *)uart_rx_buffer) == 0) {
        command = UART_COMMAND_UPTIME;
    }

    // MATLAB related commands
    else if (strcmp("matlab", (char *)uart_rx_buffer) == 0) {
        command = UART_COMMAND_MATLAB;
    }
    else if (strcmp("sample",(char *)uart_rx_buffer) == 0) {
        command = UART_COMMAND_SAMPLE;
    }
    else if (strcmp("getdata",(char *)uart_rx_buffer) == 0) {
        command = UART_COMMAND_GETDATA;
    }
    else if (strcmp("next",(char *)uart_rx_buffer) == 0) {
        command = UART_COMMAND_GETDATA_NEXT;
    }
    else if (strcmp("done",(char *)uart_rx_buffer) == 0) {
        command = UART_COMMAND_GETDATA_DONE;
    }
    else if (strcmp("bye", (char *)uart_rx_buffer) == 0) {
        command = UART_COMMAND_BYE;
    }
    else if (strcmp("start", (char *)uart_rx_buffer) == 0) {
        command = UART_COMMAND_START_STREAM;
    }
    else if (strcmp("stop", (char *)uart_rx_buffer) == 0) {
        command = UART_COMMAND_STOP_STREAM;
    }
    else if (strcmp("swirl", (char *)uart_rx_buffer) == 0) {
        command = UART_COMMAND_SWIRL;
    }
    else if (uart_rx_buffer[0] <= '9' && uart_rx_buffer[0] >= '0') {
        command = UART_COMMAND_ANGLE;
        new_hrtf_angle = atoi((char *)(uart_rx_buffer));
    }

    // No command
    else if (uart_rx_buffer[0] == '\n') {
        command = UART_COMMAND_NONE;
    }

    // reset stuff if there's actually a command
reset:
    for (i = 0; i < UART_RX_BUFFER_SIZE; i++)
        uart_rx_buffer[i] = '\0';
    sent_prompt_string = 0;
    uart_rx_buffer_index = 0;
    return command;
}

//==============================================================================
// UART TX Interrupt
//
// Used to asynchronously send data over UART
//==============================================================================
__attribute__ ((interrupt(USCIAB0TX_VECTOR)))
void uart_tx_irq(void) {
    if (uart_queue_valid) {
        if (uart_queue_sent_count == uart_queue_len) {
            uart_queue_valid = 0;
            IFG2 &= ~UCA0TXIFG;
        }
        else {
            _uart_write_byte(*(uart_queue_data + uart_queue_sent_count++));
        }
    }
    else {
        IFG2 &= ~UCA0TXIFG;
    }
}

//==============================================================================
// UART RX Interrupt
//==============================================================================
__attribute__ ((interrupt(USCIAB0RX_VECTOR)))
void uart_rx_irq(void) {
    uint8_t data = UCA0RXBUF; // reading this clears interrupt bit
    uart_rx_buffer[uart_rx_buffer_index] = (char)data;
    uart_rx_buffer_index++;
    if (uart_rx_buffer_index >= UART_RX_BUFFER_SIZE)
        uart_rx_buffer_index = UART_RX_BUFFER_SIZE - 1;
}



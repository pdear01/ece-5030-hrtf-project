/******************************************************************************\
 * Functions for using/talking to TI's TLV5616
 * ===========================================================================
 *    ECE 5030 Head Related Transfer Function
 *  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
\******************************************************************************/

#include <inttypes.h>
#include <msp430.h>
#include "hrtf.h"
#include "dac.h"

// convenient macros
#define _cs_low()  (P1OUT &= ~DAC_CS_PIN)
#define _cs_high() (P1OUT |= DAC_CS_PIN)
#define _fs_low()  (P2OUT &= ~DAC_FS_PIN)
#define _fs_high() (P2OUT |= DAC_FS_PIN)
#define _sck_low()  (P1OUT &= ~SCK_PIN)
#define _sck_high() (P1OUT |= SCK_PIN)

//==============================================================================
// Initialize DAC stuff
//==============================================================================
void dac_init(void) {
    // Initialize pin directions
    _cs_high();
    _fs_high();
    P1DIR |= DAC_CS_PIN | SCK_PIN;
    P2DIR |= DAC_IN0_PIN | DAC_IN1_PIN | DAC_FS_PIN;
}

//==============================================================================
// Enable DAC communication
//==============================================================================
void dac_enable(void) {
    dac_init();

    _fs_high();
    _sck_high();
    _cs_low();
}


//==============================================================================
// Write samples out to the DACs. This should be as fast as possible
//==============================================================================
void dac_write_samples(uint16_t sample0, uint16_t sample1) {
    _fs_low();
    // send initial 4 bit command thing
    _sck_high();
    _sck_low();
    _sck_high();
    P2OUT = (DAC_IN0_PIN | DAC_IN1_PIN);
    _sck_low();
    _sck_high();
    P2OUT = 0;
    _sck_low();
    _sck_high();
    P2OUT = 0;
    _sck_low();
    _sck_high();
    _sck_low();

    // Look here for potential speed optimizations...
    uint8_t p2out = 0;

	if (sample0 & 0x8000)
		p2out = DAC_IN0_PIN;
	if (sample1 & 0x8000)
		p2out |= DAC_IN1_PIN;
	P2OUT = p2out;
	p2out = 0;
    _sck_high();
    _sck_low();

	if (sample0 & 0x4000)
		p2out = DAC_IN0_PIN;
	if (sample1 & 0x4000)
		p2out |= DAC_IN1_PIN;
	P2OUT = p2out;
	p2out = 0;
    _sck_high();
    _sck_low();

	if (sample0 & 0x2000)
		p2out = DAC_IN0_PIN;
	if (sample1 & 0x2000)
		p2out |= DAC_IN1_PIN;
	P2OUT = p2out;
	p2out = 0;
    _sck_high();
    _sck_low();

	if (sample0 & 0x1000)
		p2out = DAC_IN0_PIN;
	if (sample1 & 0x1000)
		p2out |= DAC_IN1_PIN;
	P2OUT = p2out;
	p2out = 0;
    _sck_high();
    _sck_low();
	
	if (sample0 & 0x0800)
		p2out = DAC_IN0_PIN;
	if (sample1 & 0x0800)
		p2out |= DAC_IN1_PIN;
	P2OUT = p2out;
	p2out = 0;
    _sck_high();
    _sck_low();

	if (sample0 & 0x0400)
		p2out = DAC_IN0_PIN;
	if (sample1 & 0x0400)
		p2out |= DAC_IN1_PIN;
	P2OUT = p2out;
	p2out = 0;
    _sck_high();
    _sck_low();

	if (sample0 & 0x0200)
		p2out = DAC_IN0_PIN;
	if (sample1 & 0x0200)
		p2out |= DAC_IN1_PIN;
	P2OUT = p2out;
	p2out = 0;
    _sck_high();
    _sck_low();

	if (sample0 & 0x0100)
		p2out = DAC_IN0_PIN;
	if (sample1 & 0x0100)
		p2out |= DAC_IN1_PIN;
	P2OUT = p2out;
	p2out = 0;
    _sck_high();
    _sck_low();

	if (sample0 & 0x0080)
		p2out = DAC_IN0_PIN;
	if (sample1 & 0x0080)
		p2out |= DAC_IN1_PIN;
	P2OUT = p2out;
	p2out = 0;
    _sck_high();
    _sck_low();

	if (sample0 & 0x0040)
		p2out = DAC_IN0_PIN;
	if (sample1 & 0x0040)
		p2out |= DAC_IN1_PIN;
	P2OUT = p2out;
	p2out = 0;
    _sck_high();
    _sck_low();

	if (sample0 & 0x0020)
		p2out = DAC_IN0_PIN;
	if (sample1 & 0x0020)
		p2out |= DAC_IN1_PIN;
	P2OUT = p2out;
	p2out = 0;
    _sck_high();
    _sck_low();

	if (sample0 & 0x0010)
		p2out = DAC_IN0_PIN;
	if (sample1 & 0x0010)
		p2out |= DAC_IN1_PIN;
	P2OUT = p2out;
    _sck_high();
    _sck_low();

    _fs_high();
}

//==============================================================================
// Run this function whenever we're done using the DAC for a while
//
// Calling this won't hurt anything, so do it often to be safe (when using the
// RAM chips)
//==============================================================================
void dac_disable(void) {
    _cs_high();
    _fs_high();
}

//==============================================================================
// Produce a test sweep
//==============================================================================
void dac_run_test(void) {
    dac_enable();
    
    uint16_t i = 0;
    do {
        dac_write_samples(i, i);
        i += 0x0010;
    } while(i != 0);
    dac_write_samples(i, i);

    dac_disable();
}


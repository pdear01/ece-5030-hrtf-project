/******************************************************************************\
 * Definitions related to using internal ADC
 * ===========================================================================
 *    ECE 5030 Head Related Transfer Function
 *  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
\******************************************************************************/

#ifndef _ADC_H_
#define _ADC_H_

// Definitions
#define ANALOG_IN_1 INCH_3
#define ANALOG_IN_2 INCH_4
#define ANALOG_IN_3 INCH_5
#define ANALOG_IN_STREAMING INCH_5
#define ANALOG_REF  INCH_11

// Function prototypes
void adc_init(void);
uint8_t adc_get_sample(uint16_t);
void adc_set_channel(uint16_t channel);
int16_t adc_get_sample_fast_16(void);

#endif


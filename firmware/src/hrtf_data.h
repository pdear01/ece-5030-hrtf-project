/******************************************************************************\
 * Contains data from HRTF measurements/definitions of relevant data structures
 * ===========================================================================
 *    ECE 5030 Head Related Transfer Function
 *  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
\******************************************************************************/

#ifndef _HRTF_PARAMETERS_H_
#define _HRTF_PARAMETERS_H_

#include <inttypes.h>

// definitions
#define NUM_TIME_DELAY_BINS 360
#define NUM_AMPLITUDE_BINS  360

// hrtf parameters data structure
typedef struct _hrtf_data_t {
	// time delay related data
	int8_t sample_delays[NUM_TIME_DELAY_BINS];
	// amplitude change related data
	uint8_t amplitude_multiplier[NUM_AMPLITUDE_BINS];
} hrtf_data_t;

#endif


/******************************************************************************\
 * Functions related to using internal ADC
 * ===========================================================================
 *    ECE 5030 Head Related Transfer Function
 *  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
\******************************************************************************/

#include <inttypes.h>
#include <msp430.h>
#include "adc.h"
#include "hrtf.h"

//==============================================================================
// Initialize ADC stuff
//==============================================================================
void adc_init(void) {
    // Do voltage sampling for 500 ns (8 cycles at 16 MHz), Reference = Vcc
    ADC10CTL0 = ADC10SHT0 | ADC10ON;

    // Use SMCLK as clock source
    ADC10CTL1 = ADC10SSEL_3;

    // We use inputs A3 - A5
    ADC10AE0 = 0x38;
}

//==============================================================================
// Get sample from a channel (all 10 bits)
//
// This assumes that the channel is set correctly in the ADC registers
//==============================================================================
int16_t adc_get_sample_fast_16(void) {
    // Enable conversion, start sampling
    ADC10CTL0 |= ENC | ADC10SC;

    // Wait for conversion to finish
    while (ADC10CTL1 & ADC10BUSY);

    // Disable conversion, return result
    ADC10CTL0 &= ~ENC;
    return ADC10MEM - 0x1FF;
}

//==============================================================================
// Sets which channel to sample from
//==============================================================================
void adc_set_channel(uint16_t channel) {
    // Set channel
    ADC10CTL1 &= 0x0FFF;
    ADC10CTL1 |= channel;
}

//==============================================================================
// Get sample from a channel
// 
// Channel should be specified as INCHx value (in the correct bit positions),
// look at datasheet/user's guide, at the ADC10CTL1 register
//==============================================================================
uint8_t adc_get_sample(uint16_t channel) {
    // Set channel
    ADC10CTL1 &= 0x0FFF;
    ADC10CTL1 |= channel;

    // Enable conversion, start sampling
    ADC10CTL0 |= ENC | ADC10SC;

    // Wait for conversion to finish
    while (ADC10CTL1 & ADC10BUSY);

    // Disable conversion, return result
    ADC10CTL0 &= ~ENC;
    return (0xFF & (ADC10MEM >> 2));
}




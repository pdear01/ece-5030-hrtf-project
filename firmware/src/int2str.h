/******************************************************************************\
 * Subroutines for converting integers to strings, to be sent over debugging
 * UART
 * ===========================================================================
 *    ECE 5030 Head Related Transfer Function
 *  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
\******************************************************************************/

#ifndef _INT2STR_H_
#define _INT2STR_H_

#include <inttypes.h>

// function prototypes
void uint32_to_string(uint32_t integer, unsigned char * buffer, uint32_t buffer_len);
void int32_to_string(int32_t integer, unsigned char * buffer, uint32_t buffer_len);

#endif


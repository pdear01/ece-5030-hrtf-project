/******************************************************************************\
 * Stuff related to communicating with 23LC1024-I/P 1Mbit RAM chips
 * ===========================================================================
 *    ECE 5030 Head Related Transfer Function
 *  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
\******************************************************************************/

#include <inttypes.h>
#include <msp430.h>
#include "hrtf.h"
#include "ram.h"
#include "uart.h" // for debugging

// macros for helpfulness
#define _sck_low()  (P1OUT &= ~SCK_PIN)
#define _sck_high() (P1OUT |= SCK_PIN)
#define _cs_low()   (P1OUT &= ~CS_RAM_PIN)
#define _cs_high()  (P1OUT |= CS_RAM_PIN)
#define _set(x)     (P2OUT |= x)
#define _clr(x)     (P2OUT &= ~x)
#define _is_set(x)  (P2IN & x)

// Arrays for usefulness
const uint8_t SIO0_PIN[] = {SIO0_0_PIN, SIO0_1_PIN, SIO0_2_PIN};
const uint8_t SIO1_PIN[] = {SIO1_0_PIN, SIO1_1_PIN, SIO1_2_PIN};

//==============================================================================
// Read/Write a byte over bit-banged SPI to all three memory chips
//
// write_data/read_data should be pointers to 3 element arrays 
//
// Data is clocked out/read in MSB first
//
// Read data and write data buffers can be the same pointer~!l!1!1!11!
//==============================================================================
static void _ram_bitbang_spi_byte(uint8_t * write_data, uint8_t * read_data) {
    _sck_low();

    if (!write_data) {
        P2OUT = 0;
    }

    uint8_t i = 0x80;
    while (i != 0) {
        // Set data outputs
        if (write_data) {
            if (write_data[0] & i)
                _set(SI_0_PIN);
            else
                _clr(SI_0_PIN);
            if (write_data[1] & i)
                _set(SI_1_PIN);
            else
                _clr(SI_1_PIN);
            if (write_data[2] & i)
                _set(SI_2_PIN);
            else
                _clr(SI_2_PIN);
        }

        // Positive clock edge
        _sck_high();

        // Negative clock edge
        _sck_low();

        // Get incoming bit
        if (read_data != NULL) {
            uint8_t not_i = ~i;
            if (_is_set(SO_0_PIN))
                read_data[0] |= i;
            else
                read_data[0] &= not_i;
            if (_is_set(SO_1_PIN))
                read_data[1] |= i;
            else
                read_data[1] &= not_i;
            if (_is_set(SO_2_PIN))
                read_data[2] |= i;
            else
                read_data[2] &= not_i;
        }

        // Move on to next bit
        i >>= 1;
    }
}

//==============================================================================
// Write SDI interface for 4 cycles (1 byte)
//
// This is optimized for speed, not prettiness or size
//==============================================================================
static void _ram_write_sdi_byte(uint8_t data0, uint8_t data1, uint8_t data2) {
    uint8_t bits = 0;
    
    if (data0 & 0x80)
        bits |= SIO1_0_PIN;
    if (data0 & 0x40)
        bits |= SIO0_0_PIN;

    if (data1 & 0x80)
        bits |= SIO1_1_PIN;
    if (data1 & 0x40)
        bits |= SIO0_1_PIN;

    if (data2 & 0x80)
        bits |= SIO1_2_PIN;
    if (data2 & 0x40)
        bits |= SIO0_2_PIN;

    P2OUT = bits;
    _sck_high();
    _sck_low();

    bits = 0;

    if (data0 & 0x20)
        bits |= SIO1_0_PIN;
    if (data0 & 0x10)
        bits |= SIO0_0_PIN;

    if (data1 & 0x20)
        bits |= SIO1_1_PIN;
    if (data1 & 0x10)
        bits |= SIO0_1_PIN;

    if (data2 & 0x20)
        bits |= SIO1_2_PIN;
    if (data2 & 0x10)
        bits |= SIO0_2_PIN;

    P2OUT = bits;
    _sck_high();
    _sck_low();

    bits = 0;

    if (data0 & 0x08)
        bits |= SIO1_0_PIN;
    if (data0 & 0x04)
        bits |= SIO0_0_PIN;

    if (data1 & 0x08)
        bits |= SIO1_1_PIN;
    if (data1 & 0x04)
        bits |= SIO0_1_PIN;

    if (data2 & 0x08)
        bits |= SIO1_2_PIN;
    if (data2 & 0x04)
        bits |= SIO0_2_PIN;

    P2OUT = bits;
    _sck_high();
    _sck_low();

    bits = 0;

    if (data0 & 0x02)
        bits |= SIO1_0_PIN;
    if (data0 & 0x01)
        bits |= SIO0_0_PIN;

    if (data1 & 0x02)
        bits |= SIO1_1_PIN;
    if (data1 & 0x01)
        bits |= SIO0_1_PIN;

    if (data2 & 0x02)
        bits |= SIO1_2_PIN;
    if (data2 & 0x01)
        bits |= SIO0_2_PIN;

    P2OUT = bits;
    _sck_high();
    _sck_low();

}

//==============================================================================
// Read SDI interface for 8 cycles
//==============================================================================
static void _ram_read_sdi_byte(uint8_t * data) {
    _sck_low();

    uint8_t i, j;
    // set pin direction registers
    for (j = 0; j < 3; j++) {
        P2DIR &= ~(SIO0_PIN[j] | SIO1_PIN[j]);
    }

    for (i = 0x80; i > 0; i >>= 2) {
        uint8_t i_over_1 = i >> 1;
        for (j = 0; j < 3; j++) {
            // j corresponds to RAM chip index
            if (_is_set(SIO1_PIN[j]))
                data[j] |= i;
            else
                data[j] &= ~i;
            if (_is_set(SIO0_PIN[j]))
                data[j] |= i_over_1;
            else
                data[j] &= ~i_over_1;
        }

        _sck_high();
        _sck_low();
    }
}


//==============================================================================
// Initialize RAM chips
//==============================================================================
void ram_init(void) {
    // Setup IO pins
    P1OUT |= CS_RAM_PIN;
    P1DIR |= SCK_PIN | CS_RAM_PIN;

    uint8_t comm_buffer[3];

    // Make sure we're in SPI mode to initialise stuff
    P2DIR = 0xFF;
    _cs_low();
    _ram_write_sdi_byte(0xFF,0xFF,0xFF);
    _cs_high();
    P2DIR &= ~(SO_0_PIN | SO_1_PIN | SO_2_PIN);

    // Go to SDI mode
    _cs_low();
    comm_buffer[0] = 0x3B;
    comm_buffer[1] = 0x3B;
    comm_buffer[2] = 0x3B;
    _ram_bitbang_spi_byte(comm_buffer, NULL);
    _cs_high();
}

//==============================================================================
// Start the sequential read sequence
//==============================================================================
void ram_read_start(void) {
	// Reinitialize stuff (in case we were using the DAC)
	ram_init();

    // Set pins as outputs
    P2DIR = 0xFF;
    _sck_low();
    _cs_low();
    
    // Send read command
    _ram_write_sdi_byte(0x03,0x03,0x03);

    // send address
    _ram_write_sdi_byte(0x00,0x00,0x00);
    _ram_write_sdi_byte(0x00,0x00,0x00);
    _ram_write_sdi_byte(0x00,0x00,0x00);
    
    P2DIR = 0x00;
    _ram_write_sdi_byte(0x00,0x00,0x00);

}

//==============================================================================
// Read actual data
//==============================================================================
void ram_read_data(uint8_t * data) {
    _ram_read_sdi_byte(data);
}

//==============================================================================
// Finish reading stuff
//==============================================================================
void ram_read_finish() {
    _cs_high();
}

//==============================================================================
// Start the sequential write sequence
//==============================================================================
void ram_write_start(void) {
    // Set pins as outputs
    P2DIR = 0xFF;
    _sck_low();
    _cs_low();
    
    // Send write command
    _ram_write_sdi_byte(0x02,0x02,0x02);

    // send address
    _ram_write_sdi_byte(0x00,0x00,0x00);
    _ram_write_sdi_byte(0x00,0x00,0x00);
    _ram_write_sdi_byte(0x00,0x00,0x00);
}

//==============================================================================
// Write actual data
//==============================================================================
void ram_write_data(uint8_t data0, uint8_t data1, uint8_t data2) {
    _ram_write_sdi_byte(data0, data1, data2);
}

//==============================================================================
// Finish writing stuff
//==============================================================================
void ram_write_finish() {
    _cs_high();
}

//==============================================================================
// Run a test making sure we can write and read stuff correctly
//
// Returns 1 for success, 0 for failure
//==============================================================================
uint8_t ram_run_test(void) {
    uint32_t address = 0;
    uint8_t read_buffer[3];
    uint8_t test_data[4][3] = {{ 0xDE, 12, 0x89},
                               { 0xAD, 34, 0xAB},
                               { 0xBE, 56, 0xCD},
                               { 0xEF, 78, 0xEF}};
    ram_write_start();
    for (address = 0; address < 4; address++) {
        ram_write_data(test_data[address][0], test_data[address][1], test_data[address][2]);
    }
    ram_write_finish();

    ram_read_start();
    for (address = 0; address < 4; address++) {
        ram_read_data(read_buffer);
        if ((read_buffer[0] != test_data[address][0]) ||
            (read_buffer[1] != test_data[address][1]) ||
            (read_buffer[2] != test_data[address][2])) {
            ram_read_finish();
            uart_write_string("Address = ");
            uart_write_uint32(address);
            uart_write_string("\n");
            uart_write_string("read1 = ");
            uart_write_uint32(read_buffer[0]);
            uart_write_string("\n");
            uart_write_string("read2 = ");
            uart_write_uint32(read_buffer[1]);
            uart_write_string("\nread3 = ");
            uart_write_uint32(read_buffer[2]);
            uart_write_string("\n");
            return 0;
        }
    }
    ram_read_finish();


    return 1;
}


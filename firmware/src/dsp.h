/******************************************************************************\
 * Definitions for DSP stuff
 * ===========================================================================
 *    ECE 5030 Head Related Transfer Function
 *  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
\******************************************************************************/

#ifndef _DSP_H_
#define _DSP_H_

// definitions
#define STREAMING_BUFFER_SIZE      64 // make this a power of two, please
#define STREAMING_BUFFER_SIZE_MASK 63 // 0x3F
#define RAW_SAMPLE_OFFSET          0x1FF

#define SWIRL_MODE_THRESHOLD 8000

// Type definitions
typedef struct _dsp_state_t {
    uint16_t  hrtf_angle;
    int16_t data_buffer[STREAMING_BUFFER_SIZE];
    uint8_t  insert_index;
    uint8_t  left_index;
    uint8_t  right_index;
    uint8_t  multiplier_left;
    uint8_t  multiplier_right;
    uint8_t  use_lpf_left;
    uint8_t  use_lpf_right;
} dsp_state_t;

// function prototypes
void dsp_set_angle(dsp_state_t *, uint16_t);
void dsp_update(dsp_state_t *, uint16_t, uint16_t *, uint16_t *);
void dsp_init(dsp_state_t *);

#endif


/******************************************************************************\
 * Stuff related to communicating with 23LC1024-I/P 1Mbit RAM chips
 * ===========================================================================
 *    ECE 5030 Head Related Transfer Function
 *  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
\******************************************************************************/

#ifndef _RAM_H_
#define _RAM_H_

#define RAM_ADDR_MAX 0x1FFFF

// function prototypes
void ram_init(void);
//void ram_write_byte(uint32_t, uint8_t *);
//void ram_read_byte(uint32_t, uint8_t *);
uint8_t ram_run_test(void);

void ram_write_start();
void ram_write_data(uint8_t, uint8_t, uint8_t);
void ram_write_finish();

void ram_read_start();
void ram_read_data(uint8_t *);
void ram_read_finish();


#endif

/******************************************************************************\
 * Subroutines for converting integers to strings, to be sent over debugging
 * UART. This is here because sprintf/printf are HUGE and slow
 * ===========================================================================
 *    ECE 5030 Head Related Transfer Function
 *  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
\******************************************************************************/

#include <inttypes.h>
#include <string.h>

#include "int2str.h"

//==============================================================================
// Converts 32-bit integer to BCD integer. Based on algorithm described here:
// http://people.ee.duke.edu/~dwyer/courses/ece52/Binary_to_BCD_Converter.pdf
// 
// bcd_buffer should be at least 10 elements long
//==============================================================================
void _uint32_to_bcd(uint32_t integer, uint8_t * bcd_buffer, uint8_t * bcd_digits) {
    int8_t i, j;
    for (i = 0; i < 32; i++) {
        for (j = 0; j < 10; j++) {
            // add 3 to columns >= 5
            if (bcd_buffer[j] >= 5)
                bcd_buffer[j] += 3;
        }
        
        uint8_t shift_bit = (integer & 0x80000000) ? 1 : 0;
        integer <<= 1;
        
        for (j = 0; j < 10; j++) {
            bcd_buffer[j] <<= 1;
            bcd_buffer[j] |= shift_bit;
            shift_bit = (bcd_buffer[j] & 0x10) ? 1 : 0;
            bcd_buffer[j] &= 0x0F;
        }
    }
    *bcd_digits = 1;
    for (i = 9; i >= 0; i--) {
        if (bcd_buffer[i]) {
            *bcd_digits = i+1;
            break;
        }
    }
}

//==============================================================================
// Converts a 32-bit (signed or unsigned) integer to a string
//==============================================================================
void _int32_to_string(uint32_t integer, unsigned char * buffer, uint32_t buffer_len, uint8_t is_signed) {
    static uint8_t bcd_buffer[10];
    if (buffer_len <= 0)
        return;
    memset(bcd_buffer, 0, 10);
    uint32_t unsigned_int = is_signed ? (~integer + 1) : integer;

    uint8_t bcd_digits;
    _uint32_to_bcd(unsigned_int, bcd_buffer, &bcd_digits);

    uint8_t i;
    for (i = 0; i < buffer_len; i++) {
        if (i < bcd_digits) {
            buffer[buffer_len - i - 1] = bcd_buffer[i] + '0';
        }
        else if (i == bcd_digits && is_signed) {
            buffer[buffer_len - i - 1] = '-';
        }
        else {
            buffer[buffer_len - i - 1] = ' ';
        }
    }
}

//==============================================================================
// 'Public' wrapper functions for internal functions
//==============================================================================
void uint32_to_string(uint32_t integer, unsigned char * buffer, uint32_t buffer_len) {
    _int32_to_string(integer, buffer, buffer_len, 0);
}

void int32_to_string(int32_t integer, unsigned char * buffer, uint32_t buffer_len) {
    _int32_to_string(integer, buffer, buffer_len, 1);
}



/******************************************************************************\
 * Entry point for execution
 * ===========================================================================
 *    ECE 5030 Head Related Transfer Function
 *  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
\******************************************************************************/

#include <inttypes.h>
#include <msp430.h>
#include "hrtf.h"
#include "adc.h"
#include "uart.h"
#include "ram.h"
#include "dac.h"
#include "dsp.h"

// Global variables
volatile uint16_t uptime_seconds = 0;  // In seconds
volatile uint16_t uptime_fraction = 0;

volatile uint16_t ram_address_16b = 0;
volatile uint8_t ram_address_1b = 0; // 17th bit of address

volatile uint8_t do_sampling = 0;
volatile uint8_t do_streaming = 0;
volatile uint8_t matlab_mode = 0;
volatile uint8_t swirl_mode = 0;
volatile uint16_t swirl_mode_count = 0;

volatile uint8_t do_send_data = 0;

uint16_t new_hrtf_angle = 0;

// sampling frequency
volatile uint16_t fs = SAMPLING_FREQUENCY_STREAMING;

// use a union here to ensure we don't run out of SRAM, since we won't be using these
// buffers at the same time
union buffer_union_t {
    dsp_state_t dsp_state;
    uint8_t sample_data_buffer[GETDATA_BLOCKSIZE][3];
} buffers;

// Prototypes for functions in this file
void handle_uart(void);
void send_data(void);
void enter_streaming_mode(void);
void exit_streaming_mode(void);

//==============================================================================
// main()
//==============================================================================
int main() {
    // intialize stuff
    hrtf_init();

    while (9000) {
        // Handle received UART commands, etc
        if (!do_sampling)
            handle_uart();
        if (do_send_data) {
            send_data();
            do_send_data = 0;
        }
    }
    return 0;
}

//==============================================================================
// Timer Interrupt; set up to trigger once a sampling period
//==============================================================================
uint8_t sample1, sample2, sample3;
uint16_t streaming_samples[3];
__attribute__ ((interrupt(TIMER0_A0_VECTOR))) void ta0_handler(void) {
    // do sampling
    if (do_sampling) {
        sample1 = adc_get_sample(ANALOG_IN_1);
        sample2 = adc_get_sample(ANALOG_IN_2);
        sample3 = adc_get_sample(ANALOG_IN_3);
        ram_write_data(sample1,sample2,sample3);

        if (ram_address_16b == 0xFFFF) {
            if (ram_address_1b) {
                ram_address_1b = 0;
                ram_address_16b = 0;
                ram_write_finish();
                do_sampling = 0;
                uart_write_string("done\n");
            }
            else {
                ram_address_1b = 1;
                ram_address_16b = 0;
            }
        }
        else {
            ram_address_16b++;
        }
    }
    else if (do_streaming) {
        // get result of previous conversion
        streaming_samples[0] = ADC10MEM;
        //streaming_samples[0] = adc_get_sample(ANALOG_IN_1);
        dsp_update(&buffers.dsp_state, streaming_samples[0], streaming_samples + 1, streaming_samples + 2);

        // start next conversion
        ADC10CTL0 |= ADC10SC;
        dac_write_samples(streaming_samples[1], streaming_samples[2]);
        
        if (swirl_mode) {
            if (swirl_mode_count >= SWIRL_MODE_THRESHOLD) {
                dsp_set_angle(&buffers.dsp_state, (buffers.dsp_state.hrtf_angle + 14) % 360);
                swirl_mode_count = 0;
            }
            else {
                swirl_mode_count++;
            }
        }
    }

    // Increment uptime
    uptime_fraction++;
    if (uptime_fraction >= fs) {
        uptime_fraction = 0;
        uptime_seconds++;
    }
}

//==============================================================================
// Handle incoming UART commands
//==============================================================================
void handle_uart(void) {
    uint8_t uart_command = uart_get_command(matlab_mode);
    switch (uart_command) {
        case (UART_COMMAND_NONE): {
            break; // do nothing
        }
        case (UART_COMMAND_ANGLE): {
            swirl_mode = 0;
            uart_write_string("Setting angle to ");
            new_hrtf_angle = new_hrtf_angle % 360;
            uart_write_uint32(new_hrtf_angle);
            uart_write_char('\n');
            dsp_set_angle(&buffers.dsp_state, new_hrtf_angle);
            break;
        }
        case (UART_COMMAND_SAMPLE): {
            dac_disable();
            ram_init();
            ram_write_start();
            fs = set_timing_for_acquisition();
            do_sampling = 1;
            break;
        }
        case (UART_COMMAND_START_STREAM): {
            //uart_write_string("Starting audio streaming.\n");
            dsp_init(&buffers.dsp_state);
            adc_set_channel(ANALOG_IN_STREAMING);
            dac_enable();
            fs = set_timing_for_streaming();
            ADC10CTL0 |= ENC; // enable ADC
            do_streaming = 1;
            break;
        }
        case (UART_COMMAND_STOP_STREAM): {
            dac_disable();
            ADC10CTL0 &= ~ENC; // disable ADC conversions
            do_streaming = 0;
            uart_write_string("Stopping audio streaming.\n");
            break;
        }
        case (UART_COMMAND_TEST_ADC): {
            uint8_t sample;
            sample = adc_get_sample(ANALOG_REF);
            uart_write_string("Reference ADC value is: ");
            uart_write_uint32(sample);
            uart_write_char('\n');
            sample = adc_get_sample(ANALOG_IN_1);
            uart_write_string("CH1 ADC value is: ");
            uart_write_uint32(sample);
            uart_write_char('\n');
            sample = adc_get_sample(ANALOG_IN_2);
            uart_write_string("CH2 ADC value is: ");
            uart_write_uint32(sample);
            uart_write_char('\n');
            sample = adc_get_sample(ANALOG_IN_3);
            uart_write_string("CH3 ADC value is: ");
            uart_write_uint32(sample);
            uart_write_char('\n');
            uart_write_char('\n');
            break;
        }
        case (UART_COMMAND_TEST_RAM): {
            if (ram_run_test())
                uart_write_string("RAM test succeeded!\n");
            else
                uart_write_string("RAM test failed...\n");
            break;
        }
        case (UART_COMMAND_OVERFLOW): {
            uart_write_string("Received string exceeds input buffer length\n");
            break;
        }
        case (UART_COMMAND_TEST_DAC): {
            uart_write_string("Testing DAC...\n");
            dac_run_test();
            uart_write_string("Done\n");
            break;
        }
        case (UART_COMMAND_RESET): {
            WDTCTL = 0; // resets the chip
            break;
        }
        case (UART_COMMAND_MATLAB): {
            matlab_mode = 1;
            break;
        }
        case (UART_COMMAND_SWIRL): {
            swirl_mode = 1;
            break;
        }
        case (UART_COMMAND_GETDATA): {
            do_send_data = 1;
            break;
        }
        case (UART_COMMAND_BYE): {
            matlab_mode = 0;
            uart_write_string("Bye, MATLAB\n");
            break;
        }
        case (UART_COMMAND_UPTIME): {
            uart_write_string("The system has been alive for ");
            uart_write_uint32(uptime_seconds);
            uart_write_char('.');
            uart_write_uint32_mindigits(uptime_fraction >> 2, 4);
            uart_write_string(" seconds\n");
            break;
        }
        case (UART_COMMAND_INVALID): {
            uart_write_string("Not a valid command\n");
            break;
        }
    }
}

//==============================================================================
// Send data to MATLAB
//==============================================================================
void send_data(void) {
    uint8_t checksum;

    uint32_t base_address = 0;
    ram_read_start();
    while (base_address <= RAM_ADDR_MAX) {
        checksum = 0;
        uint8_t not_final_block = (base_address + GETDATA_BLOCKSIZE) < RAM_ADDR_MAX;

        uint16_t i;
        for (i = 0; i < GETDATA_BLOCKSIZE; i++) {
            ram_read_data(buffers.sample_data_buffer[i]);
            checksum += buffers.sample_data_buffer[i][0] +
                buffers.sample_data_buffer[i][1] + buffers.sample_data_buffer[i][2];
        }
        checksum += not_final_block;

        uint8_t go_to_next = 0;
        while (!go_to_next) {
            for (i = 0; i < GETDATA_BLOCKSIZE; i++) {
                uart_write_byte(buffers.sample_data_buffer[i][0]);
                uart_write_byte(buffers.sample_data_buffer[i][1]);
                uart_write_byte(buffers.sample_data_buffer[i][2]);
            }
            uart_write_byte(not_final_block);
            uart_write_byte(checksum);

            uint8_t uart_command = UART_COMMAND_NONE;
            while (uart_command == UART_COMMAND_NONE) {
                uart_command = uart_get_command(1);
            }

            if (uart_command == UART_COMMAND_GETDATA_NEXT ||
                uart_command == UART_COMMAND_GETDATA_DONE)
                go_to_next = 1;

        }
        base_address += GETDATA_BLOCKSIZE;
    }
    ram_read_finish();
}


/******************************************************************************\
 * HRTF project important definitions
 * ===========================================================================
 *    ECE 5030 Head Related Transfer Function
 *  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
\******************************************************************************/

#ifndef _HRTF_H_
#define _HRTF_H_

// Pins we're using
#define CS_RAM_PIN   0x01 // Pin P1.0
#define SCK_PIN      0x40 // Pin P1.6
#define DAC_CS_PIN   0x80 // Pin P1.7
#define AIN_1_PIN    0x08 // Pin P1.3
#define AIN_2_PIN    0x10 // Pin P1.4
#define AIN_3_PIN    0x20 // Pin P1.5
#define SIO0_1_PIN   0x01 // Pin P2.0
#define SI_1_PIN     SIO0_1_PIN
#define DAC_IN1_PIN  SIO0_1_PIN
#define SIO1_0_PIN   0x02 // Pin P2.1
#define SO_0_PIN     SIO1_0_PIN
#define DAC_FS_PIN   SIO1_0_PIN
#define SIO0_0_PIN   0x04 // Pin P2.2
#define SI_0_PIN     SIO0_0_PIN
#define SIO1_1_PIN   0x08 // Pin P2.3
#define SO_1_PIN     SIO1_1_PIN
#define DAC_OUT1_PIN SIO1_1_PIN
#define SIO1_2_PIN   0x10 // Pin P2.4
#define SO_2_PIN     SIO1_2_PIN
#define DAC_OUT0_PIN SIO1_2_PIN
#define SIO0_2_PIN   0x20 // Pin P2.5
#define SI_2_PIN     SIO0_2_PIN
#define DAC_IN0_PIN  SIO0_2_PIN

// Other definitions
#define SAMPLING_FREQUENCY_ACQUIRE   32000
#define SAMPLING_FREQUENCY_STREAMING 16000
#define GETDATA_BLOCKSIZE  64
#ifndef NULL
#define NULL               0
#endif

// function prototyes
void hrtf_init(void);
uint16_t set_timing_for_streaming(void);
uint16_t set_timing_for_acquisition(void);

#endif


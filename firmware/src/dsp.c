/******************************************************************************\
 * Functions for implementing HRTF real-time processing
 * ===========================================================================
 *    ECE 5030 Head Related Transfer Function
 *  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
\******************************************************************************/

#include <inttypes.h>
#include <msp430.h>
#include "hrtf.h"
#include "adc.h"
#include "uart.h"
#include "dsp.h"
#include "dac.h"
#include "hrtf_data.h"

extern const hrtf_data_t hrtf_data;

uint8_t old_multiplier_left;
uint8_t old_multiplier_right;

//==============================================================================
// Initialize DSP state structure
//==============================================================================
void dsp_init(dsp_state_t * dsp_state) {
    dsp_state->hrtf_angle = 0;
    dsp_state->insert_index = 0;
    dsp_state->left_index = STREAMING_BUFFER_SIZE;
    dsp_state->right_index = STREAMING_BUFFER_SIZE;
    dsp_state->multiplier_left = 64;
    dsp_state->multiplier_right = 64;
    dsp_state->use_lpf_left = 0;
    dsp_state->use_lpf_right = 0;
}

//==============================================================================
// Update parameters that depend on the angle
//==============================================================================
void dsp_set_angle(dsp_state_t * dsp_state, uint16_t angle) {
    //uint16_t previous_angle = dsp_state->hrtf_angle;
    dsp_state->hrtf_angle = angle;
    uint16_t right_angle = angle;
    uint16_t left_angle = (360-angle) % 360;

    // Setup amplitude change stuff
    dsp_state->multiplier_left = hrtf_data.amplitude_multiplier[left_angle];
    dsp_state->multiplier_right = hrtf_data.amplitude_multiplier[right_angle];

    // Setup indexes for time delay
    dsp_state->insert_index = 0;
    dsp_state->left_index = STREAMING_BUFFER_SIZE;
    dsp_state->right_index = STREAMING_BUFFER_SIZE + hrtf_data.sample_delays[angle];
    if (dsp_state->right_index < 0) {
        dsp_state->right_index += STREAMING_BUFFER_SIZE;
    }
    dsp_state->right_index &= STREAMING_BUFFER_SIZE_MASK;

    dsp_state->use_lpf_left = 0;
    dsp_state->use_lpf_right = 0;
    if (left_angle >= 200 && left_angle < 300) {
        dsp_state->use_lpf_left = 1;
    }
    if (right_angle >= 200 && right_angle < 300) {
        dsp_state->use_lpf_right = 1;
    }
}

//==============================================================================
// Update output based on new sample
//==============================================================================
int16_t sample_prev_left;
int16_t sample_prev_right;
void inline dsp_update(dsp_state_t * state, uint16_t input_sample,
        uint16_t * output_sample_left, uint16_t * output_sample_right) {
    state->data_buffer[state->insert_index] = (int16_t)(input_sample - RAW_SAMPLE_OFFSET);

    // Get delayed samples, scale amplitude
    int16_t sample_left = state->data_buffer[state->left_index];
    sample_left *= state->multiplier_left;
    int16_t sample_right = state->data_buffer[state->right_index];
    sample_right *= state->multiplier_right;

    // update indexes
    state->insert_index++;
    state->insert_index &= STREAMING_BUFFER_SIZE_MASK;
    state->left_index++;
    state->left_index &= STREAMING_BUFFER_SIZE_MASK;
    state->right_index++;
    state->right_index &= STREAMING_BUFFER_SIZE_MASK;
    
    // apply LPF if appropriate
    int16_t tmp;
    if (state->use_lpf_left) {
        sample_left >>= 1;
        tmp = sample_left;
        sample_left = sample_left + sample_prev_left;
        sample_prev_left = tmp;
    }
    if (state->use_lpf_right) {
        sample_right >>= 1;
        tmp = sample_right;
        sample_right = sample_right + sample_prev_right;
        sample_prev_right = tmp;
    }

    *output_sample_left  = sample_left + 0x7FFF;
    *output_sample_right = sample_right + 0x7FFF;
}



#!/usr/bin/python
# ===========================================================================
#    ECE 5030 Head Related Transfer Function
#  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import serial, time

port = serial.Serial('/dev/tty.usbserial',230400,timeout=.5,stopbits=serial.STOPBITS_ONE)
port.write('\n\n\n')
while True:
    response = port.read(100)
    text = raw_input(response);
    for c in text:
        time.sleep(.01)
        port.write(c)
        port.flush()
    time.sleep(.01)
    port.write('\n')
    time.sleep(.01)



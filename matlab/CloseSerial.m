% ===========================================================================
%    ECE 5030 Head Related Transfer Function
%  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
name = char(get(serial_obj,'Port'));                    %Get the port name of the serial object
fprintf(serial_obj,'%s\n','bye');                       %Write a command to the device to close the connection
fclose(serial_obj);                                     %Close the serial object
delete(serial_obj);                                     %Delete the serial object
fprintf('The serial port: %s has been closed.\n',name);  
clear serial_obj;                                       %Remove teh serial object from the workspace

% ===========================================================================
%    ECE 5030 Head Related Transfer Function
%  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <http://www.gnu.org/licenses/>.
%

%Clear everything that is currently stored in MATLAB
clear all;
clc;

PLOT_CHIRP = 0;
GETDATA_BLOCKSIZE = 64;

%Assuming only one COM port is being used
%Sets up the serial port to run with the following settings
avail_port = GetSerialPorts();
if ~isempty(avail_port)
    serial_obj = serial(avail_port(1,:,1));
    set(serial_obj,'BaudRate',230400);
    set(serial_obj,'FlowControl','none');
    set(serial_obj,'Parity','none');
    set(serial_obj,'DataBits',8);
    set(serial_obj,'StopBits',1);
    set(serial_obj,'Timeout',3);
    set(serial_obj,'InputBufferSize',GETDATA_BLOCKSIZE*3+2);
    fprintf('Serial Port Initialized\n');
    fprintf('\n');
    instrfind(serial_obj)
    if isunix
        serial_obj.Port = strcat('/dev/',serial_obj.Port);
    end
    fopen(serial_obj);
    fprintf(serial_obj,'%s\n','matlab');
else
    fprintf('No COM Port Detected.\n');
    fprintf('Check connections and try again.\n');
end

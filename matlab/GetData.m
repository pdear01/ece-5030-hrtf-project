% ===========================================================================
%    ECE 5030 Head Related Transfer Function
%  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <http://www.gnu.org/licenses/>.
%

%List of status flags
done = 0;
check = 0;

%Initialize variables used in this script
fs = 32000;
dt = 1/fs;
t = 0;
data = zeros(4,2^17);
addr = 0;

if (serial_obj.BytesAvailable > 0)
    fread(serial_obj,serial_obj.BytesAvailable); % clear residual data if there is any
end
fprintf(serial_obj, '%s\n', 'getdata');

num_failures = 0;

%While loop to retrieve data from SRAM chips
while ~done && num_failures < 10;
    if (mod(addr, 2048*2) == 0)
        fprintf('Reading address 0x%05X (%.2f %%)\n',addr,100*(addr / 2^17));
    end
    
    temp = fread(serial_obj);

    %Calculate the checksum of the packet and check it with the sent
    %checksum
    checksum = 0;
    for i = 1:(GETDATA_BLOCKSIZE*3)
        checksum = checksum + temp(i);
    end
    checksum = mod(checksum + temp(GETDATA_BLOCKSIZE*3 + 1), 256);
    check = (mod(checksum,256)==temp(end));
    
    %fprintf('Computed Checksum = %d, Received checksum %d\n',checksum,...
    %    temp(end));
    %disp(temp);
    
    %If the checksum is valid store the data
    if check == 1
        for i = 1:(GETDATA_BLOCKSIZE)
            bi = 3*(i-1); % base index
            
            data(:,(addr+1)) = [temp(bi+1);temp(bi+2);temp(bi+3);t];
            addr = addr + 1;
            t = t + dt;
        end
        
        %Check if there is more data available in the SRAM
        if temp(GETDATA_BLOCKSIZE*3 + 1)
            fprintf(serial_obj,'%s\n','next');
        else
            fprintf(serial_obj,'%s\n','done');
            fprintf('Done!\n');
            done = 1;
        end
    %If invalid checksum resend the data and let the user know
    else
        fprintf(serial_obj,'%s\n','invalid');
        fprintf('Got invalid checksum at address %d\n',addr);
        num_failures = num_failures + 1;
    end
end

%Write the data to a CSV file when data is done transmitting
d = datestr(now);
csvwrite(sprintf('data-%s-%s-%s.csv',d(13:14),d(16:17),d(19:20)),data);

ch1_data = data(1,:);
ch2_data = data(2,:);
ch3_data = data(3,:);
t = data(4,:);

%Plot the spectrogram of the data
figure(1);
subplot(3,1,1);
spectrogram(ch1_data,1024,120,1024,32e3);
title('Channel 1');
subplot(3,1,2);
spectrogram(ch2_data,1024,120,1024,32e3);
title('Channel 2');
subplot(3,1,3);
spectrogram(ch3_data,1024,120,1024,32e3);
title('Channel 3');

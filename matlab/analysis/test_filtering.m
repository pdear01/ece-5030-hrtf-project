%ch1_data = ch1_data(round(1.33*fs):end);
%ch2_data = ch2_data(round(1.33*fs):end);
%ch3_data = ch3_data(round(1.33*fs):end);

%offset = 0;
%sample_offset = round(offset*fs);
%ch1_len = length(ch1_data);
%td = t(sample_offset:end);
%d = [chirp(1:(ch1_len-sample_offset+1)) ch1_data(sample_offset:end)'];
[yhat H] = wienerFilter([chirp' zeros(1,length(ch3_data)-length(chirp))],ch3_data);
figure();
subplot(1,2,1);
spectrogram(yhat,1024,1000,1024,fs);
subplot(1,2,2);
spectrogram(ch3_data,1024,1000,1024,fs);
%[yhat H] = wienerFilter(d(:,1),d(:,2));
%figure();
%spectrogram(yhat,1024,1000,1024,fs);
% plot(td,d(:,1));
% hold on;
% plot(td,d(:,2),'r');
% hold off;

% chirp_clipped = chirp;
% chirp_clipped(chirp_clipped>=0) = 1;
% chirp_clipped(chirp_clipped<0) = -1;
% 
% chirp_crossings = [];
% prev_val = -1;
% prev_t = 0;
% t = 0;
% for i = 1:length(chirp_clipped)
%     if i ~= 1
%         if prev_val == -1 && chirp_clipped(i) == 1;
%             delta_t = t - prev_t;
%             chirp_crossings = [chirp_crossings delta_t];
%             prev_t = t;
%         end
%     end
%     prev_val = chirp_clipped(i);
%     t = t + 1/fs;
% end
% 
% offset = 1.35;
% data_clipped = ch3_data(round(offset*fs):end);
% data_clipped(data_clipped>=0) = 1;
% data_clipped(data_clipped<0) = -1;
% 
% data_crossings = [];
% prev_val = -1;
% prev_t = 0;
% t = 0;
% for i = 1:length(data_clipped)
%     if i ~= 1
%         if prev_val == -1 && data_clipped(i) == 1;
%             delta_t = t - prev_t;
%             data_crossings = [data_crossings delta_t];
%             prev_t = t;
%         end
%     end
%     prev_val = data_clipped(i);
%     t = t + 1/fs;
% end
% 
% loglog(chirp_crossings);
% hold on;
% loglog(data_crossings);
% hold off;


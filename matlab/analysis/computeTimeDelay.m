function delta_t = computeTimeDelay(data_L, data_R, fs)
% compute time delay between left and right ear data based on cross
% correlation

delta_t = zeros(1,20);

xcorr_1 = xcorr(data_L(1,:), data_R(1,:));
%plot(xcorr_1);
[maxval index] = max(xcorr_1);
delta_t(1) = (index - length(data_L(1,:)))/fs;

for i = 2:20;
    [maxval index] = max(xcorr(data_L(i,:), data_R(i,:)));
    delta_t(i) = -(index - length(data_L(1,:)))/fs + delta_t(1);
end
    


theta = linspace(0,359,360);
left_adj = zeros(1,length(theta));
right_adj = zeros(1,length(theta));

for i = 1:length(theta)
   [left_adj(i), right_adj(i)] = getAmplitudeChange(theta(i));
end

%plot(theta,left_adj,'b',theta,right_adj,'r');

divisors = (1:64)/64;

quantized_left_ear = zeros(1,length(theta));
for i = 1:length(theta);
    differences = abs(left_adj(i)-divisors);
    [value, index] = min(differences);
    quantized_left_ear(i) = divisors(index);
end

plot(theta, quantized_left_ear*64);
csvwrite('amplitudes.csv', quantized_left_ear*64);

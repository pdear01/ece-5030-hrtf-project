theta = linspace(0,360,1000);
left_adj = zeros(1,length(theta));
right_adj = zeros(1,length(theta));

for i = 1:length(theta)
   [left_adj(i), right_adj(i)] = getAmplitudeChange(theta(i));
end

plot(theta,left_adj,'b--',theta,right_adj,'b',...
    angles,(var_ed_nohair_L/max(var_ed_nohair_L)).^0.5,'r--',...
    angles,(var_ed_nohair_R/max(var_ed_nohair_R)).^0.5,'r');
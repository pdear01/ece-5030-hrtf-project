theta = linspace(0,360,1000);
left_adj = zeros(1,length(theta));
right_adj = zeros(1,length(theta));

for i = 1:length(theta)
   [left_adj(i), right_adj(i)] = getPowerChange(theta(i));
end

plot(theta,left_adj,'b',theta,right_adj,'r');
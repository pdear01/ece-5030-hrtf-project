function [left_adj,right_adj] = getAmplitudeChange(angle)

angle = mod(angle,360);

%Left Ear Adjustment
if angle >= 0 && angle <= 40
    left_adj = 0.01*angle + 0.61;
elseif angle > 40 && angle <= 60
    left_adj = -0.005*angle + 1.2;
elseif angle > 60 && angle <= 90
    left_adj = -0.01*angle + 1.5;
elseif angle > 90 && angle <= 100
    left_adj = -0.022*angle + 2.6;
elseif angle > 100 && angle <= 120
    left_adj = -0.003*angle + 0.72;
elseif angle > 120 && angle <= 200
    left_adj = -0.0016*angle + 0.54;
elseif angle > 200 && angle <= 300
    left_adj = 0.22;
elseif angle > 300 && angle <= 320
    left_adj = 0.0023*angle - 0.46;
elseif angle > 320 && angle <= 340
    left_adj = 0.0065*angle - 1.82;
elseif angle > 340 && angle <= 360
    left_adj = 0.0107*angle - 3.25;
end

%Right Ear Adjustment
if angle >= 0 && angle <= 40
    right_adj = -0.007*angle + 0.6;
elseif angle > 40 && angle <= 120
    right_adj = 0.28;
elseif angle > 120 && angle <= 160
    right_adj = 0.001*angle + 0.17;
elseif angle > 160 && angle <= 220
    right_adj = 0.0034*angle - 0.22;
elseif angle > 220 && angle <= 300
    right_adj = 0.0057*angle - 0.75;
elseif angle > 300 && angle <=320
    right_adj = 0.0015*angle + 0.52;
elseif angle > 320 && angle <= 340
    right_adj = -0.0137*angle + 5.39;
elseif angle > 340 && angle <= 360
    right_adj = -0.0068*angle + 3.04;
end

end
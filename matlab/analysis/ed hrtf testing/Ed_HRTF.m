function Ed_HRTF(angle,HAIR)

angle = mod(360-angle,360);     %Make the angle relative to the person
data = audioread('bell.wav');
fs = 29091;             %Each sample refers to a delay by about 23us

%Get the number of delays and which ear sound propogates too first
[num_del,dir] = getTimeDelay(angle,fs);
[adj_left,adj_right] = getPowerChange(angle);
%Check if the sound comes to the left or right ear first to apply the delay
%Note that the first column of the data vector is the left ear

%Right ear first
if dir < 0
    sam_del = zeros(abs(num_del),1);
    sound_out_left = adj_left*[sam_del' data(:,1)']';
    sound_out_right = adj_right*[data(:,2)' sam_del']';
    sound_out = [sound_out_left sound_out_right];
%Left ear first
else
    sam_del = zeros(num_del,1);
    sound_out_left = adj_left*[data(:,1)' sam_del']';
    sound_out_right = adj_right*[sam_del' data(:,2)']';
    sound_out = [sound_out_left sound_out_right];
end

%Play the sound
sound(sound_out,fs);
end
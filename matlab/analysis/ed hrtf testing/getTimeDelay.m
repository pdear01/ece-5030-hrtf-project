function [sample_delays, dir] = getTimeDelay(angle,fs)
fs = 29091;
angle = mod(angle,360);

if angle >= 0 && angle <=80
    time_delay = 9.3*angle;
elseif angle > 80 && angle <=100
    time_delay = 7*angle + 169;
elseif angle > 100 && angle <=120
    time_delay = -19.5*angle + 2828;
elseif angle > 120 && angle <=240
    time_delay = -9*angle + 1568;
elseif angle > 240 && angle <=260
    time_delay = -11*angle + 2078;
elseif angle > 260 && angle <=280
    time_delay = 4.7*angle -1984;
elseif angle > 280 && angle <=360
    time_delay = 8.2*angle - 2951;
end

dir = sign(time_delay);
sample_delays = floor(abs(time_delay)/((fs^-1)*1e6));

end
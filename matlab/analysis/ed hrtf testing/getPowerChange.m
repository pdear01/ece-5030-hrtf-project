function [left_adj,right_adj] = getPowerChange(angle)

angle = mod(angle,360);

%Left Ear Adjustment
if angle >= 0 && angle <=40
    left_adj = 1.5*angle + 36;
elseif angle > 40 && angle <=100
    left_adj = -1.4*angle + 159;
elseif angle > 100 && angle <=200
    left_adj = -0.1*angle + 27;
elseif angle > 200 && angle <=300
    left_adj = 5;
elseif angle > 300 && angle <=340
    left_adj = 0.28*angle - 79;
elseif angle > 340 && angle <=360
    left_adj = 1.1*angle - 355;
end

%Right Ear Adjustment
if angle >= 0 && angle <=40
    right_adj = -0.66*angle + 34;
elseif angle > 40 && angle <=120
    right_adj = 8;
elseif angle > 120 && angle <=180
    right_adj = 0.1*angle - 5;
elseif angle > 180 && angle <=240
    right_adj = 0.25*angle - 33;
elseif angle > 240 && angle <=300
    right_adj = angle - 223;
elseif angle > 300 && angle <=320
    right_adj = 0.3*angle + 6;
elseif angle > 320 && angle <= 360
    right_adj = -1.6*angle + 617;
end

%Convert from power to voltage (amplitude) and have it as a percentage of
%the original input
left_adj = 0.01*10*left_adj^0.5;
right_adj = 0.01*10*right_adj^0.5;

end
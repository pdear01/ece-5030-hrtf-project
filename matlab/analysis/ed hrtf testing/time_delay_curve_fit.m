theta = linspace(0,359,360);
td = zeros(1,length(theta));
samp_del = zeros(1,length(theta));
sign_del = zeros(1,length(theta));
delay_change_angles = 0;
fs = 16e3;
theta_space = linspace(0,359,360);
samp_del_space = zeros(1,length(theta_space));
sign_del_space = zeros(1,length(theta_space));

for i = 1:length(theta);
    angle = theta(i);
    if angle >= 0 && angle <= 80
        time_delay = 9.3*angle;
    elseif angle > 80 && angle <=100
        time_delay = 7*angle + 169;
    elseif angle > 100 && angle <=120
        time_delay = -19.5*angle + 2828;
    elseif angle > 120 && angle <=240
        time_delay = -9*angle + 1568;
    elseif angle > 240 && angle <=260
        time_delay = -11*angle + 2078;
    elseif angle > 260 && angle <=280
        time_delay = 4.7*angle -1984;
    elseif angle > 280 && angle <=360
        time_delay = 8.2*angle - 2951;
    end
    td(i) = time_delay;
    samp_del(i) = floor(abs(td(i))/((fs^-1)*1e6));
    sign_del(i) = sign(td(i));
end

for i = 1:length(theta_space);
    angle = theta_space(i);
    if angle >= 0 && angle <= 80
        time_delay = 9.3*angle;
    elseif angle > 80 && angle <=100
        time_delay = 7*angle + 169;
    elseif angle > 100 && angle <=120
        time_delay = -19.5*angle + 2828;
    elseif angle > 120 && angle <=240
        time_delay = -9*angle + 1568;
    elseif angle > 240 && angle <=260
        time_delay = -11*angle + 2078;
    elseif angle > 260 && angle <=280
        time_delay = 4.7*angle -1984;
    elseif angle > 280 && angle <=360
        time_delay = 8.2*angle - 2951;
    end
    time_delay;
    samp_del_space(i) = floor(abs(time_delay)/((fs^-1)*1e6));
    sign_del_space(i) = sign(td(i));
end

for i = 1:length(theta_space)
    fprintf('%d, ',theta_space(i));
end
fprintf('\n');

for i = 1:length(samp_del_space)
    fprintf('%d, ',samp_del_space(i));
end
fprintf('\n');

figure(1)
plot(theta,td,'r',angles,(delta_t_ed_nohair+delta_t_ed_withhair)/2*1e6,'b',...
    theta,sign_del_space.*(samp_del/fs*1e6),'g');
ylabel('Time Delay (us)');
xlabel('Angle (degrees)');
title('Time Delay vs Angle');
axis([0 360 -1000 1000]);
legend('Interpolated Plot','Averaged Data','Quantized Plot Relative to Fs');

% figure(2)
% plot(theta,samp_del,'b',theta,sign_del,'r');
% ylabel('Number of Samples Delayed');
% xlabel('Angle (degrees)');
% title('Sample Delay vs Angle');
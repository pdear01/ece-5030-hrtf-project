clear all; clc;
% filenames = {...
%     '../data/ed/data-reference-1.csv',...
%     '../data/ed/data-reference-2.csv'};

ed_withhair_filenames = {...
    'data-23-29-45.csv',... % 0 degrees
  	'data-23-31-39.csv',... % 20 degrees
  	'data-23-21-13.csv',... % 40 degrees
  	'data-23-22-39.csv',... % 60 degrees
  	'data-23-24-53.csv',... % 80 degrees
  	'data-23-32-36.csv',... % 90 degrees
  	'data-23-33-32.csv',... % 100 degrees
  	'data-23-34-28.csv',... % 120 degrees
  	'data-23-35-37.csv',... % 140 degrees
  	'data-23-36-29.csv',... % 160 degrees
  	'data-23-37-22.csv',... % 180 degrees
  	'data-23-38-38.csv',... % 200 degrees
  	'data-23-39-33.csv',... % 220 degrees
    'data-23-40-43.csv',... % 240 degrees
    'data-23-41-37.csv',... % 260 degrees
  	'data-23-42-34.csv',... % 270 degrees
  	'data-23-43-26.csv',... % 280 degrees
  	'data-23-27-26.csv',... % 300 degrees
  	'data-23-26-15.csv',... % 320 degrees
  	'data-23-28-41.csv'};
for i = 1:length(ed_withhair_filenames)
    ed_withhair_filenames{i} = strcat('../data/ed/withhair/',...
        ed_withhair_filenames{i});
end

[data_L data_R] = import_and_filter_data(ed_withhair_filenames);

fs = 32e3;

SPECTROGRAM_FFT_LEN = 1024;
SPECTROGRAM_WINDOW_LEN = 128*4*2;
SPECTROGRAM_OVERLAP = 128;

subplot(2,2,1);
spectrogram(data_L(1,:),SPECTROGRAM_WINDOW_LEN,...
    SPECTROGRAM_OVERLAP,SPECTROGRAM_FFT_LEN,fs);
title(sprintf('Left ear 1, postfiltering, sigma = %.2f',sqrt(var(data_L(1,:)))));

subplot(2,2,2);
spectrogram(data_L(2,:),SPECTROGRAM_WINDOW_LEN,...
    SPECTROGRAM_OVERLAP,SPECTROGRAM_FFT_LEN,fs);
title(sprintf('Left ear 2, postfiltering, sigma = %.2f',sqrt(var(data_L(2,:)))));

subplot(2,2,3);
spectrogram(data_R(1,:),SPECTROGRAM_WINDOW_LEN,...
    SPECTROGRAM_OVERLAP,SPECTROGRAM_FFT_LEN,fs);
title(sprintf('Left ear 1, postfiltering, sigma = %.2f',sqrt(var(data_R(1,:)))));

subplot(2,2,4);
spectrogram(data_R(2,:),SPECTROGRAM_WINDOW_LEN,...
    SPECTROGRAM_OVERLAP,SPECTROGRAM_FFT_LEN,fs);
title(sprintf('Left ear 2, postfiltering, sigma = %.2f',sqrt(var(data_R(2,:)))));

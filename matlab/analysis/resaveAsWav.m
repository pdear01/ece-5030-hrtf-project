function resaveAsWav(importFilename, exportFilename, fs)
% Imports data from CSV file, exports it as .wav

fprintf('Importing data from %s\n',importFilename);
data = importdata(importFilename);
ch1_data = data(1,:);
ch3_data = data(3,:);

maxval = max(ch1_data);
maxval2 = max(ch3_data);
maxval = max(maxval, maxval2);
ch1_data = (ch1_data - mean(ch1_data))/maxval;
ch3_data = (ch3_data - mean(ch3_data))/maxval;

fprintf('Exporting %s\n', exportFilename);
audiowrite(exportFilename, [ch1_data; ch3_data]', fs);

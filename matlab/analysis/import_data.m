clear all;clc;


reference_filenames = {...
    '../data/ed/data-reference-1.wav',...
    '../data/ed/data-reference-2.wav'};

[reference_L reference_R] = importAndFilterData(reference_filenames);

ref_var_L = var(reference_L(2,:));
ref_var_R = var(reference_R(2,:));

ed_withhair_filenames = {...
    'data-23-29-45.wav',... % 0 degrees
  	'data-23-31-39.wav',... % 20 degrees
  	'data-23-21-13.wav',... % 40 degrees
  	'data-23-22-39.wav',... % 60 degrees
  	'data-23-24-53.wav',... % 80 degrees
  	'data-23-32-36.wav',... % 90 degrees
  	'data-23-33-32.wav',... % 100 degrees
  	'data-23-34-28.wav',... % 120 degrees
  	'data-23-35-37.wav',... % 140 degrees
  	'data-23-36-29.wav',... % 160 degrees
  	'data-23-37-22.wav',... % 180 degrees
  	'data-23-38-38.wav',... % 200 degrees
  	'data-23-39-33.wav',... % 220 degrees
    'data-23-40-43.wav',... % 240 degrees
    'data-23-41-37.wav',... % 260 degrees
  	'data-23-42-34.wav',... % 270 degrees
  	'data-23-43-26.wav',... % 280 degrees
  	'data-23-27-26.wav',... % 300 degrees
  	'data-23-26-15.wav',... % 320 degrees
  	'data-23-28-41.wav'};
for i = 1:length(ed_withhair_filenames)
    ed_withhair_filenames{i} = strcat('../data/ed/withhair/',...
        ed_withhair_filenames{i});
end

ed_nohair_filenames = {...
    'data-00-10-14.wav',... % 0 degrees
  	'data-00-11-09.wav',... % 20 degrees
  	'data-23-51-17.wav',... % 40 degrees
  	'data-23-52-48.wav',... % 60 degrees
  	'data-23-54-02.wav',... % 80 degrees
  	'data-23-55-06.wav',... % 90 degrees
  	'data-23-56-02.wav',... % 100 degrees
  	'data-23-57-02.wav',... % 120 degrees
  	'data-23-57-55.wav',... % 140 degrees
  	'data-23-58-57.wav',... % 160 degrees
  	'data-23-59-55.wav',... % 180 degrees
  	'data-00-00-49.wav',... % 200 degrees
  	'data-00-01-55.wav',... % 220 degrees
    'data-00-02-51.wav',... % 240 degrees
    'data-00-03-44.wav',... % 260 degrees
  	'data-00-04-38.wav',... % 270 degrees
  	'data-00-05-53.wav',... % 280 degrees
  	'data-00-06-48.wav',... % 300 degrees
  	'data-00-07-53.wav',... % 320 degrees
  	'data-00-08-58.wav'};
for i = 1:length(ed_withhair_filenames)
    ed_nohair_filenames{i} = strcat('../data/ed/nohair/',...
        ed_nohair_filenames{i});
end

hanna_withglasses_filenames = {...
    'data-00-52-12.wav',... % 0 degrees
  	'data-00-53-08.wav',... % 20 degrees
  	'data-00-24-10.wav',... % 40 degrees
  	'data-00-25-19.wav',... % 60 degrees
  	'data-00-26-20.wav',... % 80 degrees
  	'data-00-27-08.wav',... % 90 degrees
  	'data-00-28-07.wav',... % 100 degrees
  	'data-00-29-36.wav',... % 120 degrees
  	'data-00-30-25.wav',... % 140 degrees
  	'data-00-40-38.wav',... % 160 degrees
  	'data-00-41-42.wav',... % 180 degrees
  	'data-00-42-43.wav',... % 200 degrees
  	'data-00-43-45.wav',... % 220 degrees
    'data-00-45-00.wav',... % 240 degrees
    'data-00-46-04.wav',... % 260 degrees
  	'data-00-46-58.wav',... % 270 degrees
  	'data-00-48-00.wav',... % 280 degrees
  	'data-00-48-59.wav',... % 300 degrees
  	'data-00-49-51.wav',... % 320 degrees
  	'data-00-51-05.wav'};
for i = 1:length(ed_withhair_filenames)
    hanna_withglasses_filenames{i} = strcat('../data/hanna/withglasses/',...
        hanna_withglasses_filenames{i});
end

hanna_noglasses_filenames = {...
    'data-00-56-08.wav',... % 0 degrees
  	'data-00-57-15.wav',... % 20 degrees
  	'data-00-58-17.wav',... % 40 degrees
  	'data-00-59-15.wav',... % 60 degrees
  	'data-01-00-46.wav',... % 80 degrees
  	'data-01-02-48.wav',... % 90 degrees
  	'data-01-04-08.wav',... % 100 degrees
  	'data-01-05-18.wav',... % 120 degrees
  	'data-01-06-40.wav',... % 140 degrees
  	'data-01-07-33.wav',... % 160 degrees
  	'data-01-08-25.wav',... % 180 degrees
  	'data-01-09-41.wav',... % 200 degrees
  	'data-01-10-51.wav',... % 220 degrees
    'data-01-13-41.wav',... % 240 degrees
    'data-01-14-41.wav',... % 260 degrees
  	'data-01-16-03.wav',... % 270 degrees
  	'data-01-17-04.wav',... % 280 degrees
  	'data-01-17-56.wav',... % 300 degrees
  	'data-01-19-07.wav',... % 320 degrees
  	'data-01-20-17.wav'};
for i = 1:length(ed_nohair_filenames)
    hanna_noglasses_filenames{i} = strcat('../data/hanna/noglasses/',...
        hanna_noglasses_filenames{i});
end

[ed_withhair_L ed_withhair_R] = importAndFilterData(ed_withhair_filenames);
[ed_nohair_L ed_nohair_R] = importAndFilterData(ed_nohair_filenames);
[hanna_withglasses_L hanna_withglasses_R] =...
    importAndFilterData(hanna_withglasses_filenames);
[hanna_noglasses_L hanna_noglasses_R] =...
    importAndFilterData(hanna_noglasses_filenames);

angles = [0, 20, 40, 60, 80, 90, 100, 120, 140,...
    160, 180, 200, 220, 240, 260, 270, 280, 300,...
    320, 340, 360];
angles_withneg = [0, 20, 40, 60, 80, 90, 100, 120, 140,...
    160, 180, -160, -140, -120, -100, -90, -80, -60,...
    -40, -20];


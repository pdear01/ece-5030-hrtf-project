clear all;clc;

fs = 32e3;
PLOT=0;
SPECTROGRAMS=1;
SPECTROGRAM_FFT_LEN = 1024;
SPECTROGRAM_WINDOW_LEN = 1024;
SPECTROGRAM_OVERLAP = 128;

fs_chirp = fs;
f_min = 50;
f_max = 10000;
duration = 3.5;
k = (f_max-f_min)/duration;
t_chirp = linspace(0,duration,duration*fs_chirp);
chirp = sin(2*pi*(f_min.*t_chirp+(k/2).*(t_chirp.^2)))';

% Import data
data = audioread('../data/ed/withhair/data-23-28-41.wav');

ch1_data = data(:,1);
%ch2_data = data(2,:);
ch3_data = data(:,2);
%t = data(4,:);

offset = 1/fs;
ch1_data = ch1_data(round(offset*fs):end);
%ch2_data = ch2_data(round(offset*fs):end);
ch3_data = ch3_data(round(offset*fs):end);
%t = t(round(offset*fs):end);

% Plot spectrograms
if SPECTROGRAMS
    figure(100);
    subplot(2,2,1);
    spectrogram(ch1_data-mean(ch1_data),SPECTROGRAM_WINDOW_LEN,...
        SPECTROGRAM_OVERLAP,SPECTROGRAM_FFT_LEN,fs);
    title(sprintf('Left ear, prefiltering, \\sigma = %.2f',sqrt(var(ch1_data))));
    view(90,270);

    subplot(2,2,3);
    spectrogram(ch3_data-mean(ch3_data),SPECTROGRAM_WINDOW_LEN,...
        SPECTROGRAM_OVERLAP,SPECTROGRAM_FFT_LEN,fs);
    title(sprintf('Right Ear, prefiltering, \\sigma = %.2f',sqrt(var(ch3_data))));
    view(90,270);
end

notch_frequencies = [60 299.3 7921 7938 419.2 538.8 134 239.5 658.7 51.1];

[b_lp,a_lp] = cheby1(9,0.5,8e3/(fs/2));
[b_hp,a_hp] = butter(6,50/(fs/2),'high');

ch1_data = filter(b_lp,a_lp, ch1_data);
%ch2_data = filter(b_lp,a_lp, ch2_data);
ch3_data = filter(b_lp,a_lp, ch3_data);

ch1_data = filter(b_hp,a_hp, ch1_data);
%ch2_data = filter(b_hp,a_hp, ch2_data);
ch3_data = filter(b_hp,a_hp, ch3_data);

for freq = notch_frequencies
    wo = freq/(fs/2);
    [b_notch a_notch] = iirnotch(wo,wo/35);
    ch1_data = filter(b_notch,a_notch, ch1_data);
    %ch2_data = filter(b_notch,a_notch, ch2_data);
    ch3_data = filter(b_notch,a_notch, ch3_data);
end

%soundsc(ch1_data,32e3);

if SPECTROGRAMS
    subplot(2,2,2);
    spectrogram(ch1_data,SPECTROGRAM_WINDOW_LEN,...
        SPECTROGRAM_OVERLAP,SPECTROGRAM_FFT_LEN,fs);
    title(sprintf('Left Ear, postfiltering, \\sigma = %.2f',sqrt(var(ch1_data))));
    view(90,270);

    subplot(2,2,4);
    spectrogram(ch3_data,SPECTROGRAM_WINDOW_LEN,...
        SPECTROGRAM_OVERLAP,SPECTROGRAM_FFT_LEN,fs);
    title(sprintf('Right Ear, postfiltering, \\sigma = %.2f',sqrt(var(ch3_data))));
    view(90,270);
end

F_ch1 = (fft(ch1_data - mean(ch1_data)))/length(ch1_data);
%F_ch2 = (fft(ch2_data - mean(ch2_data)))/length(ch2_data);
F_ch3 = (fft(ch3_data - mean(ch3_data)))/length(ch3_data);

w = linspace(0,2*pi,length(F_ch1))*fs;
f = w/2/pi;

% Plot stuff
if PLOT;
    figure(10);
    subplot(3,2,1);
    plot(t,ch1_data);
    set(gca,'XLim',[0 t(end)]);
    %set(gca,'YLim',[0 255]);
    title(sprintf('Channel 1 Data, Variance = %.3f', var(ch1_data)));
    xlabel('Time');
    ylabel('Sample Value');

    subplot(3,2,2);
    semilogx(f,abs(F_ch1));
    set(gca,'XLim',[0 fs/2]);
    title('Channel 1 FFT');
    xlabel('Frequency (Hz)');
    ylabel('Normalized Magnitude');

    subplot(2,2,3);
    plot(t,ch3_data);
    set(gca,'XLim',[0 t(end)]);
    %set(gca,'YLim',[-255 255]);
    title(sprintf('Channel 3 Data, Variance = %.3f', var(ch3_data)));
    xlabel('Time');
    ylabel('Sample Value');

    subplot(2,2,4);
    semilogx(f,abs(F_ch3));
    set(gca,'XLim',[0 fs/2]);
    title('Channel 3 FFT');
    xlabel('Frequency (Hz)');
    ylabel('Normalized Magnitude');
end

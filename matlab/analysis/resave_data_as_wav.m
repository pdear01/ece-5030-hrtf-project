clear all;clc;

fs = 32000;

reference_filenames = {...
    '../data/ed/data-reference-1.csv',...
    '../data/ed/data-reference-2.csv'};

ed_withhair_filenames = {...
    'data-23-29-45.csv',... % 0 degrees
  	'data-23-31-39.csv',... % 20 degrees
  	'data-23-21-13.csv',... % 40 degrees
  	'data-23-22-39.csv',... % 60 degrees
  	'data-23-24-53.csv',... % 80 degrees
  	'data-23-32-36.csv',... % 90 degrees
  	'data-23-33-32.csv',... % 100 degrees
  	'data-23-34-28.csv',... % 120 degrees
  	'data-23-35-37.csv',... % 140 degrees
  	'data-23-36-29.csv',... % 160 degrees
  	'data-23-37-22.csv',... % 180 degrees
  	'data-23-38-38.csv',... % 200 degrees
  	'data-23-39-33.csv',... % 220 degrees
    'data-23-40-43.csv',... % 240 degrees
    'data-23-41-37.csv',... % 260 degrees
  	'data-23-42-34.csv',... % 270 degrees
  	'data-23-43-26.csv',... % 280 degrees
  	'data-23-27-26.csv',... % 300 degrees
  	'data-23-26-15.csv',... % 320 degrees
  	'data-23-28-41.csv'};

ed_nohair_filenames = {...
    'data-00-10-14.csv',... % 0 degrees
  	'data-00-11-09.csv',... % 20 degrees
  	'data-23-51-17.csv',... % 40 degrees
  	'data-23-52-48.csv',... % 60 degrees
  	'data-23-54-02.csv',... % 80 degrees
  	'data-23-55-06.csv',... % 90 degrees
  	'data-23-56-02.csv',... % 100 degrees
  	'data-23-57-02.csv',... % 120 degrees
  	'data-23-57-55.csv',... % 140 degrees
  	'data-23-58-57.csv',... % 160 degrees
  	'data-23-59-55.csv',... % 180 degrees
  	'data-00-00-49.csv',... % 200 degrees
  	'data-00-01-55.csv',... % 220 degrees
    'data-00-02-51.csv',... % 240 degrees
    'data-00-03-44.csv',... % 260 degrees
  	'data-00-04-38.csv',... % 270 degrees
  	'data-00-05-53.csv',... % 280 degrees
  	'data-00-06-48.csv',... % 300 degrees
  	'data-00-07-53.csv',... % 320 degrees
  	'data-00-08-58.csv'};

hanna_withglasses_filenames = {...
    'data-00-52-12.csv',... % 0 degrees
  	'data-00-53-08.csv',... % 20 degrees
  	'data-00-24-10.csv',... % 40 degrees
  	'data-00-25-19.csv',... % 60 degrees
  	'data-00-26-20.csv',... % 80 degrees
  	'data-00-27-08.csv',... % 90 degrees
  	'data-00-28-07.csv',... % 100 degrees
  	'data-00-29-36.csv',... % 120 degrees
  	'data-00-30-25.csv',... % 140 degrees
  	'data-00-40-38.csv',... % 160 degrees
  	'data-00-41-42.csv',... % 180 degrees
  	'data-00-42-43.csv',... % 200 degrees
  	'data-00-43-45.csv',... % 220 degrees
    'data-00-45-00.csv',... % 240 degrees
    'data-00-46-04.csv',... % 260 degrees
  	'data-00-46-58.csv',... % 270 degrees
  	'data-00-48-00.csv',... % 280 degrees
  	'data-00-48-59.csv',... % 300 degrees
  	'data-00-49-51.csv',... % 320 degrees
  	'data-00-51-05.csv'};

hanna_noglasses_filenames = {...
    'data-00-56-08.csv',... % 0 degrees
  	'data-00-57-15.csv',... % 20 degrees
  	'data-00-58-17.csv',... % 40 degrees
  	'data-00-59-15.csv',... % 60 degrees
  	'data-01-00-46.csv',... % 80 degrees
  	'data-01-02-48.csv',... % 90 degrees
  	'data-01-04-08.csv',... % 100 degrees
  	'data-01-05-18.csv',... % 120 degrees
  	'data-01-06-40.csv',... % 140 degrees
  	'data-01-07-33.csv',... % 160 degrees
  	'data-01-08-25.csv',... % 180 degrees
  	'data-01-09-41.csv',... % 200 degrees
  	'data-01-10-51.csv',... % 220 degrees
    'data-01-13-41.csv',... % 240 degrees
    'data-01-14-41.csv',... % 260 degrees
  	'data-01-16-03.csv',... % 270 degrees
  	'data-01-17-04.csv',... % 280 degrees
  	'data-01-17-56.csv',... % 300 degrees
  	'data-01-19-07.csv',... % 320 degrees
  	'data-01-20-17.csv'};

for i = 1:length(reference_filenames)
    csvfile = reference_filenames{i};
    audiofile = csvfile(1:end-4);
    audiofile = strcat(audiofile,'.wav');
    resaveAsWav(csvfile, audiofile, fs);
end

for i = 1:length(ed_withhair_filenames)
    csvfile = ed_withhair_filenames{i};
    csvfile = strcat('../data/ed/withhair/', csvfile);
    audiofile = csvfile(1:end-4);
    audiofile = strcat(audiofile,'.wav');
    resaveAsWav(csvfile, audiofile, fs);
end

for i = 1:length(ed_nohair_filenames)
    csvfile = ed_nohair_filenames{i};
    csvfile = strcat('../data/ed/nohair/', csvfile);
    audiofile = csvfile(1:end-4);
    audiofile = strcat(audiofile,'.wav');
    resaveAsWav(csvfile, audiofile, fs);
end

for i = 1:length(hanna_withglasses_filenames)
    csvfile = hanna_withglasses_filenames{i};
    csvfile = strcat('../data/hanna/withglasses/', csvfile);
    audiofile = csvfile(1:end-4);
    audiofile = strcat(audiofile,'.wav');
    resaveAsWav(csvfile, audiofile, fs);
end

for i = 1:length(hanna_noglasses_filenames)
    csvfile = hanna_noglasses_filenames{i};
    csvfile = strcat('../data/hanna/noglasses/', csvfile);
    audiofile = csvfile(1:end-4);
    audiofile = strcat(audiofile,'.wav');
    resaveAsWav(csvfile, audiofile, fs);
end


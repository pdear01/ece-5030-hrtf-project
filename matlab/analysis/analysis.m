PLOT_SPECTRUMS = 0;
SPECTROGRAM_FFT_LEN = 1024;
SPECTROGRAM_WINDOW_LEN = 128*4*2;
SPECTROGRAM_OVERLAP = 128;
fs = 32e3;

PSD_WINDOWSIZE = fs/10; % 1 ms
PSD_NOVERLAP = PSD_WINDOWSIZE*.5; % half of window size
PSD_NFFT = 1024;
PSD_INDEX_OFFSET = 257; % corresponds to about 8kHz

DISPLAY_PLOT1 = 1; % amplitude/time plots
DISPLAY_PLOT2 = 1; % spectrograms
DISPLAY_PLOT3 = 0;
if DISPLAY_PLOT1
    % compute delta_t's
    delta_t_hanna_withglasses = computeTimeDelay(hanna_withglasses_L,...
        hanna_withglasses_R, fs);
    delta_t_hanna_withglasses = [delta_t_hanna_withglasses...
        delta_t_hanna_withglasses(1)];
    delta_t_hanna_noglasses = computeTimeDelay(hanna_noglasses_L,...
        hanna_noglasses_R, fs);
    delta_t_hanna_noglasses = [delta_t_hanna_noglasses...
        delta_t_hanna_noglasses(1)];

    delta_t_ed_withhair = computeTimeDelay(ed_withhair_L, ed_withhair_R, fs);
    delta_t_ed_withhair = [delta_t_ed_withhair delta_t_ed_withhair(1)];

    delta_t_ed_nohair = computeTimeDelay(ed_nohair_L, ed_nohair_R, fs);
    delta_t_ed_nohair = [delta_t_ed_nohair delta_t_ed_nohair(1)];

    % compute variances
    var_ed_withhair_L = zeros(1,20);
    var_ed_withhair_R = zeros(1,20);
    for i = 1:20;
        var_ed_withhair_L(i) = var(ed_withhair_L(i,:));
        var_ed_withhair_R(i) = var(ed_withhair_R(i,:));
    end
    var_ed_withhair_L = [var_ed_withhair_L...
        var_ed_withhair_L(1)];
    var_ed_withhair_R = [var_ed_withhair_R...
        var_ed_withhair_R(1)];

    var_ed_nohair_L = zeros(1,20);
    var_ed_nohair_R = zeros(1,20);
    for i = 1:20;
        var_ed_nohair_L(i) = var(ed_nohair_L(i,:));
        var_ed_nohair_R(i) = var(ed_nohair_R(i,:));
    end
    var_ed_nohair_L = [var_ed_nohair_L var_ed_nohair_L(1)];
    var_ed_nohair_R = [var_ed_nohair_R var_ed_nohair_R(1)];

    var_hanna_withglasses_L = zeros(1,20);
    var_hanna_withglasses_R = zeros(1,20);
    for i = 1:20;
        var_hanna_withglasses_L(i) = (var(hanna_withglasses_L(i,:)));
        var_hanna_withglasses_R(i) = (var(hanna_withglasses_R(i,:)));
    end
    var_hanna_withglasses_L = [var_hanna_withglasses_L var_hanna_withglasses_L(1)];
    var_hanna_withglasses_R = [var_hanna_withglasses_R var_hanna_withglasses_R(1)];

    var_hanna_noglasses_L = zeros(1,20);
    var_hanna_noglasses_R = zeros(1,20);
    for i = 1:20;
        var_hanna_noglasses_L(i) = (var(hanna_noglasses_L(i,:)));
        var_hanna_noglasses_R(i) = (var(hanna_noglasses_R(i,:)));
    end
    var_hanna_noglasses_L = [var_hanna_noglasses_L var_hanna_noglasses_L(1)];
    var_hanna_noglasses_R = [var_hanna_noglasses_R var_hanna_noglasses_R(1)];


    % Plot stuff
    var_norm_L = max(var_ed_nohair_L);
    var_norm_R = max(var_ed_nohair_R);
    subplot(2,2,1);
    plot(...
        angles, var_ed_withhair_L/var_norm_L,...
        angles, var_ed_nohair_L/var_norm_L,...
        angles, var_hanna_withglasses_L/var_norm_L,...
        angles, var_hanna_noglasses_L/var_norm_L...
        );
    set(gca, 'XLim', [0 360]);
    legend('Ed - With Hair','Ed - No Hair','Hanna - With Glasses',...
        'Hanna - No Glasses');
    title('Normalized Signal Energy in Left Ear');
    xlabel('Angle (degrees)');
    ylabel('Normalized Variance');

    subplot(2,2,2);
    plot(...
        angles, var_ed_withhair_R/var_norm_R,...
        angles, var_ed_nohair_R/var_norm_R,...
        angles, var_hanna_withglasses_R/var_norm_R,...
        angles, var_hanna_noglasses_R/var_norm_R...
        );
    set(gca, 'XLim', [0 360]);
    legend('Ed - With Hair','Ed - No Hair','Hanna - With Glasses',...
        'Hanna - No Glasses');
    title('Normalized Signal Energy in Right Ear');
    xlabel('Angle (degrees)');
    ylabel('Normalized Variance');

    subplot(2,1,2);
    plot(angles, delta_t_ed_withhair/1e-6, angles, delta_t_ed_nohair/1e-6,...
        angles, delta_t_hanna_withglasses/1e-6, angles, delta_t_hanna_noglasses/1e-6);
    set(gca, 'XLim', [0 360]);
    ylabel('Time (us)');
    xlabel('Angle (degrees)');
    title('Time Difference of Arrival in Left vs Right Ear');

end

% Compute PSDs
if DISPLAY_PLOT2
    fprintf('Evaluating power spectral densities...\n');
    psd_ed_withhair_L = [];
    fprintf('\tEd with hair, left ear\n');
    for i = 1:20
        [pxx f] = pwelch(ed_withhair_L(i,:), PSD_WINDOWSIZE,...
            PSD_NOVERLAP, PSD_NFFT, fs);
        pxx = 10*log10(pxx(1:PSD_INDEX_OFFSET));
        psd_ed_withhair_L = [psd_ed_withhair_L pxx];
    end

    fprintf('\tEd with hair, right ear\n');
    psd_ed_withhair_R = [];
    for i = 1:20
        [pxx f] = pwelch(ed_withhair_R(i,:), PSD_WINDOWSIZE,...
            PSD_NOVERLAP, PSD_NFFT, fs);
        pxx = 10*log10(pxx(1:PSD_INDEX_OFFSET));
        psd_ed_withhair_R = [psd_ed_withhair_R pxx];
    end

    fprintf('\tEd without hair, left ear\n');
    psd_ed_nohair_L = [];
    for i = 1:20
        [pxx f] = pwelch(ed_nohair_L(i,:), PSD_WINDOWSIZE,...
            PSD_NOVERLAP, PSD_NFFT, fs);
        pxx = 10*log10(pxx(1:PSD_INDEX_OFFSET));
        psd_ed_nohair_L = [psd_ed_nohair_L pxx];
    end

    fprintf('\tEd without hair, right ear\n');
    psd_ed_nohair_R = [];
    for i = 1:20
        [pxx f] = pwelch(ed_nohair_R(i,:), PSD_WINDOWSIZE,...
            PSD_NOVERLAP, PSD_NFFT, fs);
        pxx = 10*log10(pxx(1:PSD_INDEX_OFFSET));
        psd_ed_nohair_R = [psd_ed_nohair_R pxx];
    end

    fprintf('\tHanna with glasses, left ear\n');
    psd_hanna_withglasses_L = [];
    for i = 1:20
        [pxx f] = pwelch(hanna_withglasses_L(i,:), PSD_WINDOWSIZE,...
            PSD_NOVERLAP, PSD_NFFT, fs);
        pxx = 10*log10(pxx(1:PSD_INDEX_OFFSET));
        psd_hanna_withglasses_L = [psd_hanna_withglasses_L pxx];
    end

    fprintf('\tHanna with glasses, right ear\n');
    psd_hanna_withglasses_R = [];
    for i = 1:20
        [pxx f] = pwelch(hanna_withglasses_R(i,:), PSD_WINDOWSIZE,...
            PSD_NOVERLAP, PSD_NFFT, fs);
        pxx = 10*log10(pxx(1:PSD_INDEX_OFFSET));
        psd_hanna_withglasses_R = [psd_hanna_withglasses_R pxx];
    end

    fprintf('\tHanna without glasses, left ear\n');
    psd_hanna_noglasses_L = [];
    for i = 1:20
        [pxx f] = pwelch(hanna_noglasses_L(i,:), PSD_WINDOWSIZE,...
            PSD_NOVERLAP, PSD_NFFT, fs);
        pxx = 10*log10(pxx(1:PSD_INDEX_OFFSET));
        psd_hanna_noglasses_L = [psd_hanna_noglasses_L pxx];
    end

    fprintf('\tHanna without glasses, right ear\n');
    psd_hanna_noglasses_R = [];
    for i = 1:20
        [pxx f] = pwelch(hanna_noglasses_R(i,:), PSD_WINDOWSIZE,...
            PSD_NOVERLAP, PSD_NFFT, fs);
        f = f(1:PSD_INDEX_OFFSET);
        pxx = 10*log10(pxx(1:PSD_INDEX_OFFSET));
        psd_hanna_noglasses_R = [psd_hanna_noglasses_R pxx];
    end

    % Plot PSDs
    fprintf('len(f) = %d\n',length(f));
    figure(2);
    subplot(4,2,1);
    surf(angles(1:20),f,psd_ed_withhair_L,'EdgeColor','none');
    axis xy; axis tight;
    view(0,90);
    title('Ed, Hair Down, Left Ear');
    xlabel('Angle (degrees)');
    ylabel('Frequency (Hz)');

    subplot(4,2,2);
    surf(angles(1:20),f,psd_ed_withhair_R,'EdgeColor','none');
    axis xy; axis tight;
    view(0,90);
    title('Ed, Hair Down, Right Ear');
    xlabel('Angle (degrees)');
    ylabel('Frequency (Hz)');

    subplot(4,2,3);
    surf(angles(1:20),f,psd_ed_nohair_L,'EdgeColor','none');
    axis xy; axis tight;
    view(0,90);
    title('Ed, Hair Back, Left Ear');
    xlabel('Angle (degrees)');
    ylabel('Frequency (Hz)');

    subplot(4,2,4);
    surf(angles(1:20),f,psd_ed_nohair_R,'EdgeColor','none');
    axis xy; axis tight;
    view(0,90);
    title('Ed, Hair Back, Right Ear');
    xlabel('Angle (degrees)');
    ylabel('Frequency (Hz)');

    subplot(4,2,5);
    surf(angles(1:20),f,psd_hanna_withglasses_L,'EdgeColor','none');
    axis xy; axis tight;
    view(0,90);
    title('Hanna, Glasses On, Left Ear');
    xlabel('Angle (degrees)');
    ylabel('Frequency (Hz)');

    subplot(4,2,6);
    surf(angles(1:20),f,psd_hanna_withglasses_R,'EdgeColor','none');
    axis xy; axis tight;
    view(0,90);
    title('Hanna, Glasses On, Right Ear');
    xlabel('Angle (degrees)');
    ylabel('Frequency (Hz)');

    subplot(4,2,7);
    surf(angles(1:20),f,psd_hanna_noglasses_L,'EdgeColor','none');
    axis xy; axis tight;
    view(0,90);
    title('Hanna, Glasses Off, Left Ear');
    xlabel('Angle (degrees)');
    ylabel('Frequency (Hz)');

    subplot(4,2,8);
    surf(angles(1:20),f,psd_hanna_noglasses_R,'EdgeColor','none');
    axis xy; axis tight;
    view(0,90);
    title('Hanna, Glasses Off, Right Ear');
    xlabel('Angle (degrees)');
    ylabel('Frequency (Hz)');
end

if DISPLAY_PLOT3
    [pxx f] = pwelch(reference_L(1,:), PSD_WINDOWSIZE,...
            PSD_NOVERLAP, PSD_NFFT, fs);
    pxx = 10*log10(pxx);
    plot(f,pxx);
end


function [data_L_resize, data_R_resize] = importAndFilterData(list_of_files)
% Import data from a set of files, apply filtering. List of files should be
% a cell array of filenames/paths

fs = 32e3;
notch_frequencies = [60 299.3 7921 7938 419.2 538.8 134 239.5 658.7 51.1];
lp_cutoff = 10e3;
hp_cutoff = 50;
t_offset = 1;%.325;
sample_offset = round(t_offset*fs);

[b_lp, a_lp] = cheby1(9,0.5,lp_cutoff/(fs/2));
[b_hp, a_hp] = cheby1(4,0.5,hp_cutoff/(fs/2),'high');

b_notch = [];
a_notch = [];
for f = notch_frequencies;
    w0 = f/(fs/2);
    [b, a] = iirnotch(w0,w0/35);
    b_notch = [b_notch; b];
    a_notch = [a_notch; a];
end

%data_L = [];
%data_R = [];
for i = 1:length(list_of_files);
    filename = list_of_files{i};
    fprintf('Importing data from %s\n',filename);
    data = audioread(filename);
    data = data';
    
    if i == 1;
        data_L = zeros(20,length(data(1,:)));
        data_R = zeros(20,length(data(1,:)));
    end
    
    % Create useful variables
    ch1_data = data(1,:);
    %ch2_data = data(2,:);
    ch3_data = data(2,:);
    %t = data(4,:);
    
    % Cut out stuff we won't use
    ch1_data = ch1_data(sample_offset:end);
    %ch2_data = ch2_data(sample_offset:end);
    ch3_data = ch3_data(sample_offset:end);
    %t = t(sample_offset:end);
    
    % apply high/lowpass filters
    ch1_data = filter(b_hp, a_hp, ch1_data);
    %ch2_data = filter(b_hp, a_hp, ch2_data);
    ch3_data = filter(b_hp, a_hp, ch3_data);
    ch1_data = filter(b_lp, a_lp, ch1_data);
    %ch2_data = filter(b_lp, a_lp, ch2_data);
    ch3_data = filter(b_lp, a_lp, ch3_data);
    
    % Apply notch filters
    for j = 1:length(notch_frequencies);
        b = b_notch(j,:);
        a = a_notch(j,:);
        
        ch1_data = filter(b, a, ch1_data);
        %ch2_data = filter(b, a, ch2_data);
        ch3_data = filter(b, a, ch3_data);
    end
    
    if i == 1
       data_L_resize = zeros(20,length(ch1_data));
       data_R_resize = zeros(20,length(ch1_data));
    end
    
    % Channel 3 is left ear, channel 1 is right ear
    data_L_resize(i,:) = ch3_data;
    data_R_resize(i,:) = ch1_data;
end



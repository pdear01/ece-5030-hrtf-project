% Plot data/FFTs of data
% ===========================================================================
%    ECE 5030 Head Related Transfer Function
%  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <http://www.gnu.org/licenses/>.
%

%Store all the plotting vectors as variables
ch1_data = data(1,:);
ch2_data = data(2,:);
ch3_data = data(3,:);
t = data(4,:);

%Compute the FFT of each channel
F_ch1 = (fft(ch1_data - mean(ch1_data)))/length(ch1_data);
F_ch2 = (fft(ch2_data - mean(ch2_data)))/length(ch2_data);
F_ch3 = (fft(ch3_data - mean(ch3_data)))/length(ch3_data);

w = linspace(0,2*pi,length(F_ch1))*fs;
f = w/2/pi;

%Plot the Variance of Channel 1
figure(10);
subplot(3,2,1);
plot(t,ch1_data);
set(gca,'XLim',[0 t(end)]);
set(gca,'YLim',[0 255]);
title(sprintf('Channel 1 Data, Variance = %.3f', var(ch1_data)));
xlabel('Time');
ylabel('Sample Value');

%Plot the FFT of Channel 1
subplot(3,2,2);
semilogx(f,abs(F_ch1));
set(gca,'XLim',[0 fs/2]);
title('Channel 1 FFT');
xlabel('Frequency (Hz)');
ylabel('Normalized Magnitude');

%Plot the Variance of Channel 2
subplot(3,2,3);
plot(t,ch2_data);
set(gca,'XLim',[0 t(end)]);
set(gca,'YLim',[0 255]);
title(sprintf('Channel 2 Data, Variance = %.3f', var(ch2_data)));
xlabel('Time');
ylabel('Sample Value');

%Plot the FFT of Channel 2
subplot(3,2,4);
semilogx(f,abs(F_ch2));
set(gca,'XLim',[0 fs/2]);
title('Channel 2 FFT');
xlabel('Frequency (Hz)');
ylabel('Normalized Magnitude');

%Plot the Variance of Channel 3
subplot(3,2,5);
plot(t,ch3_data);
set(gca,'XLim',[0 t(end)]);
set(gca,'YLim',[0 255]);
title(sprintf('Channel 3 Data, Variance = %.3f', var(ch3_data)));
xlabel('Time');
ylabel('Sample Value');

%Plot the FFT of Channel 3
subplot(3,2,6);
semilogx(f,abs(F_ch3));
set(gca,'XLim',[0 fs/2]);
title('Channel 3 FFT');
xlabel('Frequency (Hz)');
ylabel('Normalized Magnitude');

%Plot the FFT of the chirp function
if PLOT_CHIRP
    figure(11);
    semilogx(f_chirp,F_chirp);
    set(gca,'XLim',[0 fs_chirp/2]);
    title('Chirp FFT');
    xlabel('Frequency (Hz)');
    ylabel('Normalized Magnitude');
end


% ===========================================================================
%    ECE 5030 Head Related Transfer Function
%  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <http://www.gnu.org/licenses/>.
%

fprintf(serial_obj,'%s\n', 'sample');      %Write the command to the MSP430 to start sampling data
pause(0.2);                         %Wait about 200ms before starting the chirp function

%Handle the chirp function and play the data out of one speaker
fs_chirp = 44100;
f_min = 50;
f_max = 10000;
duration = 3.5;
k = (f_max-f_min)/duration;
t_chirp = linspace(0,duration,duration*fs_chirp);
chirp = sin(2*pi*(f_min.*t_chirp+(k/2).*(t_chirp.^2)))';
chirp = [chirp zeros(length(chirp),1)];
soundsc(chirp,fs_chirp);

%Generate the FFT of the chirp
F_chirp = abs(fft(chirp(:,1)))/length(chirp(:,1));
f_chirp = linspace(0,2*pi,length(F_chirp))*fs_chirp/2/pi;

disp(fscanf(serial_obj,'%s\n'));
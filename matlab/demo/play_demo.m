clear all; clc;

fs = 16000;
duration = 1;
angles = mod(round(linspace(0, 360, 20)),360);%[0, 90, 180, 270];
t = 0:1/fs:duration;
f = [200];% 400 800 1600 3200 6400 12800];
x = zeros(1,length(t));
for i = 1:length(f);
    x = x + cos(2*pi*f(i)*t)/length(f);
end
x = hann(length(t))'.*x;

amplitudes = csvread('amplitudes.csv');
delays = csvread('delays.csv');

max_delay = max(abs(delays));

for i = 1:length(angles)
    angle = angles(i);
    
    fprintf('Playing sound for %d degrees\n',angle);
    
    right_angle = angle;
    left_angle = mod(360-angle, 360);
    
    amplitude_L = amplitudes(left_angle + 1);
    amplitude_R = amplitudes(right_angle + 1);
    
    amp_max = max(amplitude_L, amplitude_R);
    
    delay = delays(angle+1);
    
    
    left = amplitude_L*x(max_delay+1:end-max_delay-delay)/amp_max;
    right = amplitude_R*x(max_delay+delay+1:end-max_delay)/amp_max;
    
    if (left_angle >= 200 && left_angle < 300);
        left = filter([1 1]/2, 1, left);
    end
    if (right_angle >= 200 && right_angle < 300);
        right = filter([1 1]/2, 1, right);
    end
    
    player = audioplayer([left; right], fs);
    player.playblocking();
    
end


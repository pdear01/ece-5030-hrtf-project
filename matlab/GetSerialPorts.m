%Function to find the COM ports because MATLAB does not have a built in
%functino for this. Once MATLAB starts, MATLAB assumes the COM ports will
%never change from those values. This function uses the built in terminal
%of the operating system
% ===========================================================================
%    ECE 5030 Head Related Transfer Function
%  Copyright (C) 2013, Patrick Dear, Ed Szoka, Hanna Lin
%
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
function available_ports = GetSerialPorts()

%Find what serial ports are available based on the OS that the program is
%using
if ispc
    %Use cmd to find the COM ports because MATLAB does not do this on the
    %fly
    [ret_val, ret_str] = system('reg query HKLM\HARDWARE\DEVICEMAP\SERIALCOMM');
    ret_str = ret_str(60:length(ret_str));
    available_ports = zeros(100,4);
    num_ports = 0;
    
    %Traverse the string to find what the valid serial ports are
    for i = 1:length(ret_str)-3
        if strcmp('COM',ret_str(i:i+2))
            num_ports = num_ports + 1;
            available_ports(num_ports,:) = strcat('COM',ret_str(i+3));
        end
    end
    %Get rid of all the nonused elements in the array and have the array be
    %characters
    available_ports = available_ports(1:num_ports,:);
    available_ports = char(available_ports);
    
elseif isunix 
    %Use terminal to determine if the OS is Linux or Mac
    [ret_val, ret_str] = system('uname');
    if strcmpi(strcat(ret_str),'Linux')
        %Running Linux
        [ret_val, ret_str] = system('ls /dev | grep ttyUSB');
        
        %Initialize some data
        available_ports = zeros(100,7);
        num_ports = 0;
        
        %Find the number of COM ports available
        for i = 1:length(ret_str)/8
            available_ports(i,:) = strcat(ret_str(1+(i-1)*8:i*8));
            num_ports = num_ports + 1;
        end
        
        %Modify the matrix to make it a character vector with the list of
        %usable COM ports
        available_ports = available_ports(1:num_ports,:);
        available_ports = char(available_ports);
    else
        %Running Mac OS
        [ret_val, ret_str] = system('ls /dev | grep tty.usbserial');
        
        %This is currently only set up to run with one USB-Serial Converter
        %plugged in at a time. Will fix this in later revisions when more
        %time is available
        available_ports = char(ret_str);
    end
        
else
    fprintf('Get a PC or a UNIX system');
end

end


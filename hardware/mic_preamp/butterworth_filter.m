% want a second-order sallen-key butterworth filter
clear all;clc;
j = sqrt(-1);

f0 = 20e3;
w0 = 2*pi*f0;

Q = 1/sqrt(2); % corresponds to butterworth

C1 = .047e-6;
C2 = .0047e-6;

% solve R2^2 - R2/(w0*Q*C2) + 1/(w0^2*C1*C2) = 0
b = -1/(w0*Q*C2);
c = 1/(w0^2*C1*C2);
R2 = -b/2 + sqrt(b^2-4*c)/2;

R1 = 1/(C2*Q*w0) - R2;

if imag(R2) ~= 0;
    fprintf('Computed imaginary resistor values. This is an issue.\n');
    disp(R2);
end

fprintf('For cutoff of %.3f kHz and Q = %.3f:\n',f0,Q);
fprintf('\tC1 = %.3f nF\n',C1/1e-9);
fprintf('\tC2 = %.3f nF\n',C2/1e-9);
fprintf('\tR1 = %.3f Ohms\n',R1);
fprintf('\tR2 = %.3f Ohms\n',R2);

R1 = 120;
R2 = 2.2e3;
w0_final = 1/sqrt(C1*C2*R1*R2);
f0_final = w0_final / 2 / pi;
Q_final = 1/(C2*(R1+R2)*w0_final);

% Plot stuff
w = linspace(10,2*pi*1e5,1000);
H = @(s) 1./(s.^2*C1*C2*R1*R2 + s*C2*(R1+R2) + 1);
H_s = H(j*w);

PLOT = 1;
if PLOT ~= 0;
    mag = 20*log10(abs(H_s));
    subplot(2,1,1);
    semilogx(w/2/pi,mag);
    xlabel('Frequency (Hz)');
    ylabel('Magnitude (dB)');
    set(gca,'YLim',[-30 5]);
    set(gca,'XLim',[10 1e5]);
    phase = atan2(imag(H_s),real(H_s))*180/pi;
    subplot(2,1,2);
    semilogx(w/2/pi,phase);
    set(gca,'YLim',[-180 10]);
    set(gca,'XLim',[10 1e5]);
    xlabel('Frequency (Hz)');
end
